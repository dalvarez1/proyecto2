/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */


#include "scanner.h"//se importa el header del analisis sintactico
#include "metodoW.h"
#include "LISTA.h"
#include <QLinkedList>
#include <iostream> //libreria para imprimir en cosola de C
#include "mainwindow.h"
#include <QString> //libreria para manejo de STRINGS de QT
#include <tsimbolo.h>
#include <QHash> //Libreria para manejar HASH TABLES de QT, se usa para la tabla de simbolos
#include <QDebug>
#include <QTextEdit> //libreria QTextEdit de QT para poder mostrar el resultado en pantalla
#include "tablasimbolos.h" //clase que contiene los valores de la tabla de simbolos

extern int yylineno; //linea actual donde se encuentra el parser (analisis lexico) lo maneja BISON
extern int columna; //columna actual donde se encuentra el parser (analisis lexico) lo maneja BISON
extern char *yytext; //lexema actual donde esta el parser (analisis lexico) lo maneja BISON

QLinkedList<QString> lista;
QHash <QString, QString> tabla_s; //TABLA DE SIMBOLOS

metodoW *waz;
QLinkedList<tablasimbolos> *tablasimbolo=new QLinkedList<tablasimbolos>();
tablasimbolos *s;
	tsimbolo * t;
void setTabla(tsimbolo*ts){
t=ts;
}
QLinkedList<tablasimbolos> *gettodo(){
return tablasimbolo;
}

int yyerror(const char* mens){
//metodo que se llama al haber un error sintactico
//SE IMPRIME EN CONSOLA EL ERROR
std::cout <<mens<<" "<<yytext<< std::endl;
return 0;
}

QLinkedList<QString> getlita(){
return lista;
}




# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sintactico.h".  */
#ifndef YY_YY_SINTACTICO_H_INCLUDED
# define YY_YY_SINTACTICO_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mas = 258,
     menos = 259,
     por = 260,
     divis = 261,
     mod = 262,
     coma = 263,
     igual = 264,
     print = 265,
     pcoma = 266,
     aparen = 267,
     cparen = 268,
     cllave = 269,
     allave = 270,
     menor = 271,
     mayor = 272,
     menori = 273,
     mayori = 274,
     iguali = 275,
     dif = 276,
     diferente = 277,
     Or = 278,
     And = 279,
     IF = 280,
     Else = 281,
     Case = 282,
     Switch = 283,
     For = 284,
     While = 285,
     Break = 286,
     color = 287,
     path = 288,
     Class = 289,
     draw = 290,
     resize = 291,
     rotate = 292,
     move = 293,
     circunscribe = 294,
     erase = 295,
     comilla = 296,
     p = 297,
     Variables = 298,
     Figures = 299,
     Design = 300,
     Int = 301,
     String = 302,
     Float = 303,
     Arreglo = 304,
     textura = 305,
     blue = 306,
     orange = 307,
     red = 308,
     brownie = 309,
     green = 310,
     yellow = 311,
     white = 312,
     black = 313,
     skyblue = 314,
     grey = 315,
     triangulo = 316,
     dosp = 317,
     New = 318,
     ovalo = 319,
     poligono = 320,
     circulo = 321,
     Name = 322,
     posx = 323,
     posy = 324,
     Grade = 325,
     Base = 326,
     lineaguion = 327,
     lineapunto = 328,
     lineaguionpunto = 329,
     lineasolida = 330,
     RootFig = 331,
     ChildFig = 332,
     Height = 333,
     flecha = 334,
     rectangulo = 335,
     nube = 336,
     punto = 337,
     default = 338,
     texto = 339,
     linea = 340,
     imagen = 341,
     iden = 342,
     Cadena = 343,
     acorch = 344,
     ccorch = 345,
     numerod = 346,
     numero = 347
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{


//se especifican los tipo de valores para los no terminales y lo terminales
char TEXT [256];
float real;
char*str;
QLinkedList<LISTA> *LIS;
LISTA *LISTA;



} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_SINTACTICO_H_INCLUDED  */

/* Copy the second part of user declarations.  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   912

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  93
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  44
/* YYNRULES -- Number of rules.  */
#define YYNRULES  209
/* YYNRULES -- Number of states.  */
#define YYNSTATES  549

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   347

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     9,    14,    19,    24,    29,    35,    41,
      47,    52,    57,    62,    65,    67,    75,    84,    96,   107,
     109,   121,   129,   131,   134,   136,   144,   156,   171,   183,
     195,   201,   204,   207,   210,   213,   216,   219,   222,   225,
     228,   231,   234,   237,   239,   241,   243,   245,   247,   249,
     251,   253,   255,   257,   259,   261,   269,   270,   280,   290,
     300,   310,   320,   330,   340,   350,   360,   370,   380,   389,
     395,   401,   407,   413,   419,   425,   431,   437,   443,   449,
     453,   457,   461,   465,   469,   473,   477,   481,   485,   489,
     490,   492,   494,   496,   498,   500,   502,   504,   506,   508,
     510,   512,   514,   516,   518,   528,   537,   541,   545,   555,
     564,   569,   573,   577,   582,   586,   590,   594,   598,   601,
     607,   611,   613,   615,   617,   619,   621,   623,   626,   628,
     630,   634,   635,   638,   642,   646,   650,   654,   658,   662,
     664,   666,   670,   674,   679,   681,   682,   685,   687,   690,
     693,   695,   697,   702,   707,   713,   719,   723,   732,   737,
     741,   745,   750,   751,   757,   763,   769,   773,   775,   777,
     781,   783,   785,   789,   792,   794,   796,   802,   806,   810,
     816,   820,   822,   828,   832,   836,   838,   844,   858,   870,
     874,   878,   882,   888,   890,   896,   900,   904,   906,   910,
     914,   916,   920,   924,   928,   930,   932,   936,   939,   941
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
      94,     0,    -1,    34,    87,    15,    95,    14,    -1,    34,
      15,    95,    14,    -1,    43,    15,    14,    95,    -1,    44,
      15,    14,    95,    -1,    45,    15,    14,    95,    -1,    43,
      15,   123,    14,    95,    -1,    44,    15,    99,    14,    95,
      -1,    45,    15,    96,    14,    95,    -1,    43,    15,   123,
      14,    -1,    44,    15,    99,    14,    -1,    45,    15,    96,
      14,    -1,    96,    97,    -1,    97,    -1,    25,    12,   118,
      13,    15,    96,    14,    -1,    25,    12,   118,    13,    15,
      96,    14,   117,    -1,    25,    12,   118,    13,    15,    96,
      14,    26,    15,    96,    14,    -1,    28,    12,    87,    13,
      15,   115,    83,    62,    96,    14,    -1,   134,    -1,    29,
      12,   123,    11,   118,    11,   116,    13,    15,    96,    14,
      -1,    30,    12,   118,    13,    15,    96,    14,    -1,   123,
      -1,    98,    97,    -1,    98,    -1,    40,    12,    67,    62,
      87,    13,    11,    -1,    39,    12,    76,    62,    87,     8,
      77,    62,    87,    13,    11,    -1,    38,    12,    67,    62,
      87,     8,    69,    62,   120,     8,    68,   120,    13,    11,
      -1,    37,    12,    67,    62,    87,     8,    70,    62,   120,
      13,    11,    -1,    36,    12,    71,    62,   120,     8,    78,
      62,   120,    13,    11,    -1,    35,    12,    87,    13,    11,
      -1,   100,    99,    -1,   101,    99,    -1,   102,    99,    -1,
     103,    99,    -1,   104,    99,    -1,   105,    99,    -1,   106,
      99,    -1,   107,    99,    -1,   108,    99,    -1,   109,    99,
      -1,   110,    99,    -1,   111,    99,    -1,   110,    -1,   111,
      -1,   100,    -1,   101,    -1,   102,    -1,   103,    -1,   104,
      -1,   105,    -1,   106,    -1,   107,    -1,   108,    -1,   109,
      -1,    87,    42,    87,     9,    87,    11,    99,    -1,    -1,
      61,    87,     9,    63,    61,    12,   112,    13,    11,    -1,
      64,    87,     9,    63,    64,    12,   112,    13,    11,    -1,
      81,    87,     9,    63,    81,    12,   112,    13,    11,    -1,
      66,    87,     9,    63,    66,    12,   112,    13,    11,    -1,
      84,    87,     9,    63,    84,    12,   112,    13,    11,    -1,
      86,    87,     9,    63,    86,    12,   112,    13,    11,    -1,
      79,    87,     9,    63,    79,    12,   112,    13,    11,    -1,
      85,    87,     9,    63,    85,    12,   112,    13,    11,    -1,
      65,    87,     9,    63,    65,    12,   112,    13,    11,    -1,
      80,    87,     9,    63,    80,    12,   112,    13,    11,    -1,
      50,    87,     9,    63,    50,    12,   112,    13,    11,    -1,
      50,    87,     9,    63,    50,    12,    13,    11,    -1,    32,
      62,   113,     8,   112,    -1,    33,    62,    87,     8,   112,
      -1,    85,    62,   114,     8,   112,    -1,    50,    62,    87,
       8,   112,    -1,    50,    62,   113,     8,   112,    -1,    71,
      62,   134,     8,   112,    -1,    70,    62,   134,     8,   112,
      -1,    68,    62,   134,     8,   112,    -1,    69,    62,   134,
       8,   112,    -1,    78,    62,   134,     8,   112,    -1,    32,
      62,   113,    -1,    33,    62,    87,    -1,    85,    62,   114,
      -1,    50,    62,    87,    -1,    50,    62,   113,    -1,    71,
      62,   134,    -1,    70,    62,   134,    -1,    68,    62,   134,
      -1,    69,    62,   134,    -1,    78,    62,   134,    -1,    -1,
      51,    -1,    52,    -1,    53,    -1,    54,    -1,    55,    -1,
      56,    -1,    57,    -1,    58,    -1,    59,    -1,    60,    -1,
      75,    -1,    72,    -1,    73,    -1,    74,    -1,    27,    87,
      62,    15,    96,    31,    11,    14,   115,    -1,    27,    87,
      62,    15,    96,    31,    11,    14,    -1,    87,     3,     3,
      -1,    87,     4,     4,    -1,    26,    25,    12,   118,    13,
      15,    96,    14,   117,    -1,    26,    25,    12,   118,    13,
      15,    96,    14,    -1,    26,    15,    96,    14,    -1,   134,
      18,   134,    -1,   134,    20,   134,    -1,   134,    21,     9,
     134,    -1,   134,    16,   134,    -1,   134,    17,   134,    -1,
     134,    23,   134,    -1,   134,    24,   134,    -1,   119,   118,
      -1,    12,   118,    13,   119,   118,    -1,    12,   118,    13,
      -1,   118,    -1,    19,    -1,    18,    -1,    17,    -1,    16,
      -1,    20,    -1,    21,     9,    -1,    23,    -1,    24,    -1,
      12,   119,    13,    -1,    -1,     4,   120,    -1,   120,     3,
     120,    -1,   120,     4,   120,    -1,   120,     5,   120,    -1,
     120,     6,   120,    -1,   120,     7,   120,    -1,    12,   120,
      13,    -1,    92,    -1,    87,    -1,   121,     3,   121,    -1,
      41,    87,    41,    -1,    41,    87,   122,    41,    -1,    87,
      -1,    -1,    87,   122,    -1,    87,    -1,   124,   123,    -1,
     125,   123,    -1,   124,    -1,   125,    -1,    87,     9,   134,
      11,    -1,    87,     9,   121,    11,    -1,    87,     9,   134,
      11,   123,    -1,    87,     9,   121,    11,   123,    -1,    87,
       9,   134,    -1,    50,    87,     9,    63,    50,    12,    13,
      11,    -1,    46,   126,    11,   124,    -1,    47,   127,    11,
      -1,    48,   128,    11,    -1,    87,     9,   134,    11,    -1,
      -1,    46,    87,     9,    92,    11,    -1,    47,    87,     9,
     121,    11,    -1,    48,    87,     9,    91,    11,    -1,    87,
       8,   126,    -1,    87,    -1,   129,    -1,    87,     8,   127,
      -1,    87,    -1,   130,    -1,    87,     8,   128,    -1,    87,
     128,    -1,    87,    -1,   131,    -1,    87,     9,    92,     8,
     129,    -1,    87,     8,   129,    -1,    87,     9,   134,    -1,
      87,     9,   134,     8,   129,    -1,    87,     9,    87,    -1,
     134,    -1,    87,     9,   121,     8,   130,    -1,    87,     8,
     130,    -1,    87,     9,   121,    -1,    87,    -1,    87,     9,
      91,     8,   131,    -1,    87,    89,   134,    90,    89,   134,
      90,     9,    89,   132,    90,     8,   131,    -1,    87,    89,
     134,    90,    89,   134,    90,     9,    89,   132,    90,    -1,
      87,     9,    91,    -1,    87,     8,   131,    -1,    87,     9,
     134,    -1,    87,     9,   134,     8,   131,    -1,    87,    -1,
      12,   133,    13,     8,   132,    -1,    12,   133,    13,    -1,
      92,     8,   133,    -1,    92,    -1,   134,     3,   135,    -1,
     134,     4,   135,    -1,   135,    -1,   135,     5,   136,    -1,
     135,     6,   136,    -1,   135,     7,   136,    -1,   136,    -1,
      92,    -1,    12,   134,    13,    -1,     4,   134,    -1,    91,
      -1,    87,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   211,   211,   212,   214,   215,   216,   217,   218,   219,
     220,   221,   222,   225,   226,   228,   231,   232,   233,   234,
     235,   236,   240,   241,   242,   246,   247,   248,   249,   250,
     251,   254,   255,   256,   257,   258,   259,   260,   261,   262,
     263,   264,   265,   266,   267,   268,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,   297,
     298,   299,   300,   301,   302,   303,   304,   305,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   315,   316,   317,
     326,   327,   328,   329,   330,   331,   332,   333,   334,   335,
     337,   337,   337,   337,   338,   339,   342,   342,   344,   345,
     346,   350,   351,   352,   353,   362,   363,   364,   365,   366,
     367,   368,   372,   373,   374,   375,   376,   377,   378,   379,
     380,   381,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   397,   398,   399,   400,   401,   403,   404,   406,   407,
     408,   409,   410,   413,   416,   419,   422,   426,   428,   429,
     430,   431,   434,   437,   441,   442,   444,   445,   446,   448,
     449,   450,   452,   453,   454,   455,   457,   458,   459,   462,
     463,   472,   474,   475,   476,   477,   479,   480,   481,   482,
     483,   484,   485,   486,   488,   489,   492,   493,   496,   497,
     502,   505,   506,   507,   508,   512,   513,   514,   515,   516
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "mas", "menos", "por", "divis", "mod",
  "coma", "igual", "print", "pcoma", "aparen", "cparen", "cllave",
  "allave", "menor", "mayor", "menori", "mayori", "iguali", "dif",
  "diferente", "Or", "And", "IF", "Else", "Case", "Switch", "For", "While",
  "Break", "color", "path", "Class", "draw", "resize", "rotate", "move",
  "circunscribe", "erase", "comilla", "p", "Variables", "Figures",
  "Design", "Int", "String", "Float", "Arreglo", "textura", "blue",
  "orange", "red", "brownie", "green", "yellow", "white", "black",
  "skyblue", "grey", "triangulo", "dosp", "New", "ovalo", "poligono",
  "circulo", "Name", "posx", "posy", "Grade", "Base", "lineaguion",
  "lineapunto", "lineaguionpunto", "lineasolida", "RootFig", "ChildFig",
  "Height", "flecha", "rectangulo", "nube", "punto", "default", "texto",
  "linea", "imagen", "iden", "Cadena", "acorch", "ccorch", "numerod",
  "numero", "$accept", "INICIO", "CUERPO", "INSTRUCCIONES", "INSTRUCCION",
  "INSTRUCCIONF", "FIG", "TRIANGULO", "OVALO", "NUBE", "CIRCULO", "TEXTO",
  "IMAGEN", "FLECHA", "LINEA", "POLIGONO", "RECTANGULO", "TEXTURA",
  "TEXTURA1", "LPARAMETROS", "LCOLOR", "TLINEA", "CASOS", "INCREMENTO",
  "ELSI", "EXPRESIONLOGICA", "EXPRE", "EXPRESIONNUMERICA",
  "EXPRESIONCADENA", "CADLARGA", "ASIGNACION", "DECLARACION", "ASIGNAR",
  "ASIGM", "ASIGM1", "ASIGM2", "VALORES", "VALORES1", "VALORES2",
  "ARREGLO", "A", "E", "T", "F", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    93,    94,    94,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    96,    96,    97,    97,    97,    97,    97,
      97,    97,    97,    97,    97,    98,    98,    98,    98,    98,
      98,    99,    99,    99,    99,    99,    99,    99,    99,    99,
      99,    99,    99,    99,    99,    99,    99,    99,    99,    99,
      99,    99,    99,    99,    99,    99,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     112,   112,   112,   112,   112,   112,   112,   112,   112,   112,
     112,   112,   112,   112,   112,   112,   112,   112,   112,   112,
     113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
     114,   114,   114,   114,   115,   115,   116,   116,   117,   117,
     117,   118,   118,   118,   118,   118,   118,   118,   118,   118,
     118,   118,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   120,   120,   120,   120,   120,   120,   120,   120,
     120,   121,   121,   121,   121,   121,   122,   122,   123,   123,
     123,   123,   123,   123,   123,   123,   123,   110,   124,   124,
     124,   124,   124,   125,   125,   125,   126,   126,   126,   127,
     127,   127,   128,   128,   128,   128,   129,   129,   129,   129,
     129,   129,   130,   130,   130,   130,   131,   131,   131,   131,
     131,   131,   131,   131,   132,   132,   133,   133,   134,   134,
     134,   135,   135,   135,   135,   136,   136,   136,   136,   136
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     5,     4,     4,     4,     4,     5,     5,     5,
       4,     4,     4,     2,     1,     7,     8,    11,    10,     1,
      11,     7,     1,     2,     1,     7,    11,    14,    11,    11,
       5,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     7,     0,     9,     9,     9,
       9,     9,     9,     9,     9,     9,     9,     9,     8,     5,
       5,     5,     5,     5,     5,     5,     5,     5,     5,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     0,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     9,     8,     3,     3,     9,     8,
       4,     3,     3,     4,     3,     3,     3,     3,     2,     5,
       3,     1,     1,     1,     1,     1,     1,     2,     1,     1,
       3,     0,     2,     3,     3,     3,     3,     3,     3,     1,
       1,     3,     3,     4,     1,     0,     2,     1,     2,     2,
       1,     1,     4,     4,     5,     5,     3,     8,     4,     3,
       3,     4,     0,     5,     5,     5,     3,     1,     1,     3,
       1,     1,     3,     2,     1,     1,     5,     3,     3,     5,
       3,     1,     5,     3,     3,     1,     5,    13,    11,     3,
       3,     3,     5,     1,     5,     3,     3,     1,     3,     3,
       1,     3,     3,     3,     1,     1,     3,     2,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     0,     1,     0,     0,     0,     0,
       0,     0,     0,     0,     3,     0,     0,     0,     0,     0,
       0,     0,   150,   151,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    43,    44,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   209,   208,   205,     0,    14,    24,    22,
      19,   200,   204,     2,     4,   209,     0,   168,   181,   170,
       0,   171,   174,     0,   175,   145,    10,   148,   149,     5,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    11,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,   209,   207,     0,     6,     0,
       0,   162,     0,     0,     0,     0,     0,     0,     0,    12,
      13,    23,     0,     0,     0,     0,     0,     0,     0,   162,
       0,   145,   159,     0,     0,   174,     0,   173,   160,     0,
     209,     0,   156,     7,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,   206,   131,   125,
     124,   123,   122,   126,     0,   128,   129,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     9,
     198,   199,   201,   202,   203,   209,   166,   168,   209,   205,
     178,     0,     0,     0,     0,   158,   170,   169,   171,   144,
       0,   172,   175,   208,   191,     0,     0,     0,   145,   153,
     152,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   127,     0,   118,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   163,     0,     0,
     145,     0,   164,     0,   165,     0,   208,     0,   142,   147,
       0,   141,   155,   154,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   131,   130,   162,   114,
     115,   111,   112,     0,   116,   117,     0,     0,   162,    30,
       0,     0,   140,   139,     0,     0,     0,     0,     0,   205,
     209,   176,   179,     0,   184,   185,   182,   193,   186,   192,
       0,   146,   143,     0,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    56,   131,     0,     0,   113,     0,
       0,     0,     0,   132,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   161,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    55,     0,   119,    15,     0,     0,     0,     0,    21,
     138,   133,   134,   135,   136,   137,     0,     0,     0,     0,
      25,   177,   183,   190,     0,    68,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,   162,
       0,     0,     0,     0,     0,     0,     0,     0,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,    79,    80,
      82,    83,    86,    87,    85,    84,    88,   101,   102,   103,
     100,    81,    67,    57,    58,    65,    60,    63,    66,    59,
      61,    64,    62,   162,     0,   162,     0,   106,   107,   162,
       0,     0,     0,     0,     0,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,     0,     0,     0,    18,     0,
       0,     0,     0,     0,     0,     0,    69,    70,    72,    73,
      76,    77,    75,    74,    78,    71,    17,     0,     0,    20,
      29,    28,     0,    26,   197,     0,   188,     0,     0,     0,
       0,   195,     0,   162,   105,     0,   196,     0,   187,     0,
     104,    27,   194,   109,     0,   108,   162,     0,   110
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     9,    66,    67,    68,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,   370,
     448,   461,   340,   388,   427,   238,   178,   304,   151,   270,
      69,    22,    23,    76,    80,    83,    77,    81,    84,   505,
     525,    70,    71,    72
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -293
static const yytype_int16 yypact[] =
{
     -24,    -8,    26,   405,    62,  -293,    81,   105,   125,    69,
     405,   150,   735,   253,  -293,   119,   405,    66,   -14,    74,
     135,   142,   145,   145,   405,    89,    92,   103,   123,   134,
     158,   160,   167,   169,   181,   183,   107,   156,   496,   496,
     496,   496,   496,   496,   496,   496,   496,   496,   496,   496,
      97,    97,   405,   268,   282,   290,   304,   314,   346,   351,
     353,   358,   379,   135,  -293,  -293,   313,  -293,   744,  -293,
      29,   516,  -293,  -293,  -293,   227,   370,  -293,    29,   118,
     387,  -293,    50,   409,  -293,    59,   405,  -293,  -293,  -293,
     421,   425,   442,   453,   459,   468,   469,   486,   487,   494,
     495,   341,   405,  -293,  -293,  -293,  -293,  -293,  -293,  -293,
    -293,  -293,  -293,  -293,  -293,  -293,  -293,    61,  -293,    95,
     385,   145,    95,   406,   435,   447,   449,   441,   457,   405,
    -293,  -293,    97,    97,    97,    97,    97,   159,   161,   250,
     438,   -12,  -293,   448,   168,    58,    97,  -293,  -293,   458,
      99,   261,   319,  -293,   464,   465,   488,   489,   490,   491,
     492,   493,   502,   503,   504,   541,  -293,  -293,    95,  -293,
    -293,  -293,  -293,  -293,   549,  -293,  -293,   555,    95,   825,
     556,   560,   559,   561,   511,   517,   522,   523,   524,  -293,
     516,   516,  -293,  -293,  -293,   474,  -293,  -293,   576,   206,
     208,   159,   438,   448,   580,  -293,   186,  -293,  -293,  -293,
     258,  -293,  -293,   391,   302,   171,    27,    10,   -12,   145,
     145,   540,   537,   536,   543,   544,   532,   535,   538,   533,
     531,   542,   534,   605,    68,   737,  -293,   607,  -293,    97,
      97,    97,    97,   617,    97,    97,   612,    95,   614,   634,
     291,   557,   562,   563,   564,   237,   275,  -293,   275,    97,
     -12,   565,  -293,   567,  -293,   567,   401,   558,  -293,   568,
     615,  -293,  -293,  -293,   636,   648,   649,   650,   651,   652,
     653,   654,   656,   669,   677,   686,   839,  -293,   744,    29,
      29,    29,    29,    97,    29,    29,   672,   689,   744,  -293,
     291,   291,  -293,  -293,   381,   696,   697,   699,   695,   702,
     266,  -293,  -293,   365,    87,   276,  -293,    85,  -293,  -293,
      97,  -293,  -293,   755,   286,   286,   286,   286,   286,   286,
     286,   286,   286,   286,   496,   440,    95,   378,    29,   624,
     632,   629,   407,   527,   308,   291,   291,   291,   291,   291,
     639,   663,   665,   641,   725,   275,  -293,   565,   567,    46,
     726,   676,   681,   682,   683,   690,   700,   701,   703,   704,
     738,   746,   751,   754,   757,   764,   765,   773,   776,   780,
     781,  -293,   782,  -293,   713,   736,   740,    57,   784,  -293,
    -293,   527,   527,   301,   328,   328,   741,   742,   750,   756,
    -293,  -293,  -293,  -293,   797,  -293,   813,   743,   619,    97,
      97,    97,    97,    97,   366,   802,   816,   821,   823,   826,
     827,   828,   833,   836,   842,   843,    83,  -293,   846,   744,
     871,   872,   860,   291,   291,   291,   790,   789,  -293,  -293,
    -293,  -293,  -293,  -293,  -293,  -293,  -293,  -293,   873,   874,
     875,   876,   369,   371,   419,   463,   471,  -293,  -293,  -293,
    -293,   877,  -293,  -293,  -293,  -293,  -293,  -293,  -293,  -293,
    -293,  -293,  -293,   744,   867,   744,   472,  -293,  -293,   744,
     633,   722,   484,   878,   868,   286,   286,   286,   286,   286,
     286,   286,   286,   286,   286,   501,    95,   566,  -293,   595,
     879,   881,   818,   882,   795,   798,  -293,  -293,  -293,  -293,
    -293,  -293,  -293,  -293,  -293,  -293,  -293,   883,   884,  -293,
    -293,  -293,   291,  -293,   886,   885,   889,   887,   890,   804,
     795,   891,   567,   744,   672,   892,  -293,   868,  -293,   655,
    -293,  -293,  -293,   863,   152,  -293,   744,   684,  -293
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -293,  -293,    52,  -260,   -66,  -293,   -25,  -293,  -293,  -293,
    -293,  -293,  -293,  -293,  -293,  -293,  -293,  -293,  -293,  -285,
     497,  -293,   367,  -293,   357,  -116,  -157,  -292,  -136,   637,
      14,   768,  -293,   771,   769,    23,  -133,  -113,  -131,   373,
     382,   -16,   214,   408
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -190
static const yytype_int16 yytable[] =
{
     130,    78,   131,   177,   197,   210,   182,     3,   343,   344,
       1,   234,   212,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    21,     5,   208,   337,   149,
     132,   133,   132,   133,   116,   117,    87,    88,   342,   371,
     372,   373,   374,   375,   376,   377,   378,   379,   380,   132,
     133,   268,   233,   391,   392,   393,   394,   395,   143,   144,
     430,   431,    15,    50,   132,   133,   143,   215,    74,   152,
      50,    51,    50,    79,   167,   209,    89,    10,    51,     4,
     168,   287,   271,    14,   169,   170,   171,   172,   173,   174,
     218,   175,   176,   358,   215,   261,    11,   269,   473,    50,
     149,    50,  -144,   179,   118,   147,   179,   168,   474,    51,
    -144,   169,   170,   171,   172,   173,   174,   267,   175,   176,
      12,    78,   200,   311,   314,   312,   140,   141,   214,   336,
     216,   297,   318,    73,   319,   181,   404,   145,   153,   146,
      13,   480,   481,   482,    85,   145,   150,   146,   316,   101,
      64,    65,   235,    75,   166,   115,    86,    64,    65,    64,
      65,    82,   179,    50,    16,    50,   211,   546,   147,   476,
     102,    51,    50,    51,   146,    50,    90,   474,   382,    91,
      51,   189,   115,    51,   115,    78,    64,    65,    64,    65,
      92,    17,    18,    19,   140,   260,    17,    18,    19,   214,
     506,   507,   508,   509,   510,   511,   512,   513,   514,   515,
      93,   132,   133,   495,   256,   497,   258,   257,   179,   499,
     383,    94,   401,   289,   290,   291,   292,   403,   294,   295,
     529,   179,    20,   272,   273,   137,   138,    20,  -167,   200,
      78,    50,    78,   313,   402,    95,   195,    96,   198,    51,
      64,    65,    64,   199,    97,   115,    98,    50,   115,   213,
      65,   218,   266,    65,   218,    51,   261,    52,    99,   262,
     100,   130,   219,   539,   355,   255,   130,   338,    53,    50,
     119,    54,    55,    56,   357,   260,   547,    51,    57,    58,
      59,    60,    61,    62,   120,   300,   201,   202,   203,    17,
      18,    19,   121,   301,   359,   132,   133,   348,   349,   381,
     265,   345,   346,   347,   348,   349,   122,    50,   361,   362,
     179,   390,   132,   133,   198,    51,   123,   129,    64,   309,
     220,   345,   346,   347,   348,   349,   363,   204,    53,    78,
      63,    54,    55,    56,    64,    65,   190,   191,    57,    58,
      59,    60,    61,    62,   364,   365,   366,   367,   124,    17,
      18,    19,   310,   125,   368,   126,    64,    65,   132,   133,
     127,   369,   132,   133,   132,   133,   356,   489,   302,   490,
     517,   139,    50,   303,   345,   346,   347,   348,   349,   350,
      51,   128,   384,   452,   453,   454,   455,   456,   142,   263,
      63,   538,   264,    53,    64,    65,    54,    55,    56,   263,
     130,    50,  -189,    57,    58,    59,    60,    61,    62,    51,
     148,   389,   132,   133,    17,    18,    19,   491,   165,   130,
     154,   130,    53,   130,   155,    54,    55,    56,   457,   458,
     459,   460,    57,    58,    59,    60,    61,    62,     6,     7,
       8,   156,   335,    17,    18,    19,   169,   170,   171,   172,
     173,   174,   157,   175,   176,    63,   132,   133,   158,    64,
      65,   492,   180,   130,   132,   133,    50,   159,   160,   493,
     179,   130,   137,   255,    51,  -167,   498,   345,   346,   347,
     348,   349,   502,   183,    63,   161,   162,    53,    64,    65,
      54,    55,    56,   163,   164,    50,   184,    57,    58,    59,
      60,    61,    62,    51,   185,   516,   186,   187,    17,    18,
      19,   134,   135,   136,   188,   206,    53,   221,   222,    54,
      55,    56,   347,   348,   349,   145,    57,    58,    59,    60,
      61,    62,   192,   193,   194,   217,    25,    17,    18,    19,
     232,   223,   224,   225,   226,   227,   228,    26,   236,    63,
      27,    28,    29,    64,    65,   229,   230,   231,   237,   246,
      50,   247,   248,   250,   249,    30,    31,    32,    51,   251,
      33,    34,    35,    36,   252,   253,   254,  -180,    63,   259,
     274,    53,    64,    65,    54,    55,    56,   518,   275,    50,
     276,    57,    58,    59,    60,    61,    62,    51,   277,   519,
     278,   279,    17,    18,    19,   280,   283,   282,   286,   281,
      53,   285,   288,    54,    55,    56,   293,   296,   284,   298,
      57,    58,    59,    60,    61,    62,   345,   346,   347,   348,
     349,    17,    18,    19,   305,   299,   500,   320,   323,   306,
     307,   308,   315,    63,   317,   269,   322,    64,    65,    50,
     324,   325,   326,   327,   328,   329,   330,    51,   331,   543,
     438,   439,   440,   441,   442,   443,   444,   445,   446,   447,
      53,   332,    63,    54,    55,    56,    64,    65,    50,   333,
      57,    58,    59,    60,    61,    62,    51,   334,   548,   339,
     341,    17,    18,    19,   351,   352,   450,   353,   354,    53,
     256,   385,    54,    55,    56,   386,   387,   396,   399,    57,
      58,    59,    60,    61,    62,   345,   346,   347,   348,   349,
      17,    18,    19,   397,   398,   501,   400,   405,   406,   426,
     132,   133,    63,   407,   408,   409,    64,    65,    50,    24,
     167,   415,   410,   239,   240,   241,    51,   242,   243,   416,
     244,   245,   411,   412,   417,   413,   414,   418,   360,    53,
     419,    63,    54,    55,    56,    64,    65,   420,   421,    57,
      58,    59,    60,    61,    62,    25,   422,   361,   362,   423,
      17,    18,    19,   424,   425,   287,    26,   432,   428,    27,
      28,    29,   429,   433,   434,   363,   437,   345,   346,   347,
     348,   349,   435,   462,    30,    31,    32,   535,   436,    33,
      34,    35,    36,   364,   365,   366,   367,   463,   132,   133,
     449,    63,   464,   368,   465,    64,    65,   466,   467,   468,
     369,   239,   240,   241,   469,   242,   243,   470,   244,   245,
    -120,   335,  -120,   471,   472,   169,   170,   171,   172,   173,
     174,   475,   175,   176,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   477,   479,   478,   483,   484,   496,
     504,   485,   486,   487,   488,   494,   522,   524,   526,   544,
     520,   503,   521,   523,   530,   528,   527,   532,   531,   537,
     545,   540,   533,   541,   534,   451,   321,   205,   196,   207,
     542,     0,   536
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-293)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      66,    17,    68,   119,   137,   141,   122,    15,   300,   301,
      34,   168,   143,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    11,     0,   140,   288,    41,
       3,     4,     3,     4,    50,    51,    22,    23,   298,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,     3,
       4,    41,   168,   345,   346,   347,   348,   349,     8,     9,
       3,     4,    10,     4,     3,     4,     8,     9,    16,    85,
       4,    12,     4,    87,    13,    87,    24,    15,    12,    87,
      12,    13,   218,    14,    16,    17,    18,    19,    20,    21,
       3,    23,    24,     8,     9,     8,    15,    87,    15,     4,
      41,     4,     3,   119,    52,    82,   122,    12,    25,    12,
      11,    16,    17,    18,    19,    20,    21,    90,    23,    24,
      15,   137,   138,   256,   260,   258,     8,     9,   144,   286,
     146,   247,   263,    14,   265,   121,    90,    87,    86,    89,
      15,   433,   434,   435,     9,    87,    87,    89,   261,    42,
      91,    92,   168,    87,   102,    87,    14,    91,    92,    91,
      92,    87,   178,     4,    14,     4,   143,    15,   145,   429,
      14,    12,     4,    12,    89,     4,    87,    25,   335,    87,
      12,   129,    87,    12,    87,   201,    91,    92,    91,    92,
      87,    46,    47,    48,     8,     9,    46,    47,    48,   215,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
      87,     3,     4,   473,     8,   475,     8,    11,   234,   479,
     336,    87,   355,   239,   240,   241,   242,   358,   244,   245,
     522,   247,    87,   219,   220,     8,     9,    87,    11,   255,
     256,     4,   258,   259,   357,    87,    87,    87,    87,    12,
      91,    92,    91,    92,    87,    87,    87,     4,    87,    91,
      92,     3,    91,    92,     3,    12,     8,    14,    87,    11,
      87,   337,    11,   533,     8,     9,   342,   293,    25,     4,
      12,    28,    29,    30,     8,     9,   546,    12,    35,    36,
      37,    38,    39,    40,    12,     4,    46,    47,    48,    46,
      47,    48,    12,    12,   320,     3,     4,     6,     7,   334,
       8,     3,     4,     5,     6,     7,    12,     4,    32,    33,
     336,    13,     3,     4,    87,    12,    12,    14,    91,    92,
      11,     3,     4,     5,     6,     7,    50,    87,    25,   355,
      87,    28,    29,    30,    91,    92,   132,   133,    35,    36,
      37,    38,    39,    40,    68,    69,    70,    71,    12,    46,
      47,    48,    87,    12,    78,    12,    91,    92,     3,     4,
      12,    85,     3,     4,     3,     4,    11,     8,    87,     8,
     496,    11,     4,    92,     3,     4,     5,     6,     7,     8,
      12,    12,    14,   409,   410,   411,   412,   413,    11,     8,
      87,   532,    11,    25,    91,    92,    28,    29,    30,     8,
     476,     4,    11,    35,    36,    37,    38,    39,    40,    12,
      11,    14,     3,     4,    46,    47,    48,     8,    87,   495,
       9,   497,    25,   499,     9,    28,    29,    30,    72,    73,
      74,    75,    35,    36,    37,    38,    39,    40,    43,    44,
      45,     9,    12,    46,    47,    48,    16,    17,    18,    19,
      20,    21,     9,    23,    24,    87,     3,     4,     9,    91,
      92,     8,    87,   539,     3,     4,     4,     9,     9,     8,
     496,   547,     8,     9,    12,    11,    14,     3,     4,     5,
       6,     7,     8,    87,    87,     9,     9,    25,    91,    92,
      28,    29,    30,     9,     9,     4,    71,    35,    36,    37,
      38,    39,    40,    12,    67,    14,    67,    76,    46,    47,
      48,     5,     6,     7,    67,    87,    25,    63,    63,    28,
      29,    30,     5,     6,     7,    87,    35,    36,    37,    38,
      39,    40,   134,   135,   136,    87,    50,    46,    47,    48,
       9,    63,    63,    63,    63,    63,    63,    61,     9,    87,
      64,    65,    66,    91,    92,    63,    63,    63,    13,    13,
       4,    11,    13,    62,    13,    79,    80,    81,    12,    62,
      84,    85,    86,    87,    62,    62,    62,    11,    87,     9,
      50,    25,    91,    92,    28,    29,    30,    31,    61,     4,
      64,    35,    36,    37,    38,    39,    40,    12,    65,    14,
      66,    79,    46,    47,    48,    80,    85,    84,    13,    81,
      25,    87,    15,    28,    29,    30,     9,    15,    86,    15,
      35,    36,    37,    38,    39,    40,     3,     4,     5,     6,
       7,    46,    47,    48,    87,    11,    13,    89,    12,    87,
      87,    87,    87,    87,    87,    87,    41,    91,    92,     4,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    14,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      25,    12,    87,    28,    29,    30,    91,    92,     4,    12,
      35,    36,    37,    38,    39,    40,    12,    11,    14,    27,
      11,    46,    47,    48,     8,     8,    87,     8,    13,    25,
       8,    87,    28,    29,    30,    83,    87,    78,    77,    35,
      36,    37,    38,    39,    40,     3,     4,     5,     6,     7,
      46,    47,    48,    70,    69,    13,    11,    11,    62,    26,
       3,     4,    87,    62,    62,    62,    91,    92,     4,    14,
      13,    13,    62,    16,    17,    18,    12,    20,    21,    13,
      23,    24,    62,    62,    13,    62,    62,    13,    13,    25,
      13,    87,    28,    29,    30,    91,    92,    13,    13,    35,
      36,    37,    38,    39,    40,    50,    13,    32,    33,    13,
      46,    47,    48,    13,    13,    13,    61,    13,    62,    64,
      65,    66,    62,    62,    62,    50,     9,     3,     4,     5,
       6,     7,    62,    11,    79,    80,    81,    13,    62,    84,
      85,    86,    87,    68,    69,    70,    71,    11,     3,     4,
      87,    87,    11,    78,    11,    91,    92,    11,    11,    11,
      85,    16,    17,    18,    11,    20,    21,    11,    23,    24,
      11,    12,    13,    11,    11,    16,    17,    18,    19,    20,
      21,    15,    23,    24,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,     3,    15,     4,    87,    89,    12,
      12,     8,     8,     8,     8,     8,    68,    92,    90,    26,
      11,    13,    11,    11,     8,    11,    13,     8,    13,     8,
     543,   534,    15,    11,    14,   408,   269,   139,   137,   140,
     537,    -1,   530
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    34,    94,    15,    87,     0,    43,    44,    45,    95,
      15,    15,    15,    15,    14,    95,    14,    46,    47,    48,
      87,   123,   124,   125,    14,    50,    61,    64,    65,    66,
      79,    80,    81,    84,    85,    86,    87,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
       4,    12,    14,    25,    28,    29,    30,    35,    36,    37,
      38,    39,    40,    87,    91,    92,    96,    97,    98,   123,
     134,   135,   136,    14,    95,    87,   126,   129,   134,    87,
     127,   130,    87,   128,   131,     9,    14,   123,   123,    95,
      87,    87,    87,    87,    87,    87,    87,    87,    87,    87,
      87,    42,    14,    99,    99,    99,    99,    99,    99,    99,
      99,    99,    99,    99,    99,    87,   134,   134,    95,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    14,
      97,    97,     3,     4,     5,     6,     7,     8,     9,    11,
       8,     9,    11,     8,     9,    87,    89,   128,    11,    41,
      87,   121,   134,    95,     9,     9,     9,     9,     9,     9,
       9,     9,     9,     9,     9,    87,    95,    13,    12,    16,
      17,    18,    19,    20,    21,    23,    24,   118,   119,   134,
      87,   123,   118,    87,    71,    67,    67,    76,    67,    95,
     135,   135,   136,   136,   136,    87,   126,   129,    87,    92,
     134,    46,    47,    48,    87,   124,    87,   127,   130,    87,
     121,   128,   131,    91,   134,     9,   134,    87,     3,    11,
      11,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    63,     9,   118,   119,   134,     9,    13,   118,    16,
      17,    18,    20,    21,    23,    24,    13,    11,    13,    13,
      62,    62,    62,    62,    62,     9,     8,    11,     8,     9,
       9,     8,    11,     8,    11,     8,    91,    90,    41,    87,
     122,   121,   123,   123,    50,    61,    64,    65,    66,    79,
      80,    81,    84,    85,    86,    87,    13,    13,    15,   134,
     134,   134,   134,     9,   134,   134,    15,   118,    15,    11,
       4,    12,    87,    92,   120,    87,    87,    87,    87,    92,
      87,   129,   129,   134,   121,    87,   130,    87,   131,   131,
      89,   122,    41,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    11,    12,   119,    96,   134,    27,
     115,    11,    96,   120,   120,     3,     4,     5,     6,     7,
       8,     8,     8,     8,    13,     8,    11,     8,     8,   134,
      13,    32,    33,    50,    68,    69,    70,    71,    78,    85,
     112,   112,   112,   112,   112,   112,   112,   112,   112,   112,
     112,    99,   119,   118,    14,    87,    83,    87,   116,    14,
      13,   120,   120,   120,   120,   120,    78,    70,    69,    77,
      11,   129,   130,   131,    90,    11,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    26,   117,    62,    62,
       3,     4,    13,    62,    62,    62,    62,     9,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,   113,    87,
      87,   113,   134,   134,   134,   134,   134,    72,    73,    74,
      75,   114,    11,    11,    11,    11,    11,    11,    11,    11,
      11,    11,    11,    15,    25,    15,    96,     3,     4,    15,
     120,   120,   120,    87,    89,     8,     8,     8,     8,     8,
       8,     8,     8,     8,     8,    96,    12,    96,    14,    96,
      13,    13,     8,    13,    12,   132,   112,   112,   112,   112,
     112,   112,   112,   112,   112,   112,    14,   118,    31,    14,
      11,    11,    68,    11,    92,   133,    90,    13,    11,   120,
       8,    13,     8,    15,    14,    13,   133,     8,   131,    96,
     115,    11,   132,    14,    26,   117,    15,    96,    14
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 15:

    {
t->insertar((s=new tablasimbolos((yyvsp[(6) - (7)].real),QString::number((yyvsp[(3) - (7)].LIS)),"IF")),s->getNom());
}
    break;

  case 21:

    {
//$$ = new metodoW(QString b,QLinkedList() c);

}
    break;

  case 111:

    {}
    break;

  case 114:

    {QString te=QString::number((yyvsp[(1) - (3)].real));
 QString te1=QString::number((yyvsp[(3) - (3)].real));
 QString sig= "<";
 qDebug()<<te+sig+te1;
QString ds=te+sig+te1;
 //QByteArray array = ds.toLocal8Bit();
 (yyval.LIS)= ds;
 qDebug()<<ds;
 }
    break;

  case 152:

    {
t->insertar((s=new tablasimbolos((yyvsp[(3) - (4)].real),QString((yyvsp[(1) - (4)].TEXT)),"id")),s->getNom());
}
    break;

  case 153:

    {
t->insertar((s=new tablasimbolos((yyvsp[(3) - (4)].real),QString((yyvsp[(1) - (4)].TEXT)),"id")),s->getNom());
}
    break;

  case 154:

    {
t->insertar((s=new tablasimbolos((yyvsp[(3) - (5)].real),QString((yyvsp[(1) - (5)].TEXT)),"id")),s->getNom());
}
    break;

  case 155:

    {
t->insertar((s=new tablasimbolos((yyvsp[(3) - (5)].real),QString((yyvsp[(1) - (5)].TEXT)),"id")),s->getNom());
}
    break;

  case 156:

    {t->setValor(QString::number((yyvsp[(3) - (3)].real)),(yyvsp[(1) - (3)].TEXT));}
    break;

  case 161:

    {t->setValor(QString::number((yyvsp[(3) - (4)].real)),(yyvsp[(1) - (4)].TEXT));
qDebug()<<"ahora si entro";
}
    break;

  case 163:

    {
t->insertar((s=new tablasimbolos((yyvsp[(4) - (5)].real),QString((yyvsp[(2) - (5)].TEXT)),"id")),s->getNom());
//qDebug()<<t->size();
}
    break;

  case 167:

    {t->insertar((s=new tablasimbolos(0,QString((yyvsp[(1) - (1)].TEXT)),"id")),s->getNom());}
    break;

  case 176:

    {t->insertar((s=new tablasimbolos((yyvsp[(3) - (5)].real),QString((yyvsp[(1) - (5)].TEXT)),"id")),s->getNom());}
    break;

  case 178:

    {t->insertar((s=new tablasimbolos((yyvsp[(3) - (3)].real),QString((yyvsp[(1) - (3)].TEXT)),"id")),s->getNom());
qDebug()<<"aki hace la suma d variables con numeros";
}
    break;

  case 179:

    {t->insertar((s=new tablasimbolos((yyvsp[(3) - (5)].real),QString((yyvsp[(1) - (5)].TEXT)),"id")),s->getNom());}
    break;

  case 180:

    {
if(t->buscar((yyvsp[(3) - (3)].TEXT))!="null"){
 t->insertar((s=new tablasimbolos((t->buscar((yyvsp[(3) - (3)].TEXT)).toInt()),QString((yyvsp[(1) - (3)].TEXT)),"id")),s->getNom());
}else{
qDebug()<<"la variable :";
qDebug()<<(yyvsp[(3) - (3)].TEXT);
qDebug()<<"no ha sido declarada"; 
}
}
    break;

  case 181:

    {}
    break;

  case 198:

    {(yyval.real)=(yyvsp[(1) - (3)].real)+(yyvsp[(3) - (3)].real);}
    break;

  case 199:

    {(yyval.real)=(yyvsp[(1) - (3)].real)-(yyvsp[(3) - (3)].real);
            QString temp=QString::number((yyval.real));
			lista.append(temp);
			
			}
    break;

  case 200:

    {(yyval.real)=(yyvsp[(1) - (1)].real);
			}
    break;

  case 201:

    {(yyval.real)=(yyvsp[(1) - (3)].real)*(yyvsp[(3) - (3)].real);}
    break;

  case 202:

    {(yyval.real)=(yyvsp[(1) - (3)].real)/(yyvsp[(3) - (3)].real);}
    break;

  case 204:

    {(yyval.real)=(yyvsp[(1) - (1)].real);

}
    break;

  case 205:

    {}
    break;

  case 207:

    {(yyval.real)=-(yyvsp[(2) - (2)].real);}
    break;

  case 208:

    {}
    break;

  case 209:

    {
(yyval.real)=atof((t->buscar((yyvsp[(1) - (1)].TEXT))).toStdString().c_str());//convertir de string a char

}
    break;



      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}





