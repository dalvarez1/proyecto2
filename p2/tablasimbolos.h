#ifndef TABLASIMBOLOS_H
#define TABLASIMBOLOS_H
#include <QString>

class tablasimbolos
{
public:
    tablasimbolos(int valor, QString nom, QString b);
     int valor;
    QString nom;
    int getValor();
    int buscar(int b);
    QString getNom();
    void setValor(int a);
};
#endif //  TABLASIMBOLOS_H
