/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_SINTACTICO_H_INCLUDED
# define YY_YY_SINTACTICO_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mas = 258,
     menos = 259,
     por = 260,
     divis = 261,
     mod = 262,
     coma = 263,
     igual = 264,
     print = 265,
     pcoma = 266,
     aparen = 267,
     cparen = 268,
     cllave = 269,
     allave = 270,
     menor = 271,
     mayor = 272,
     menori = 273,
     mayori = 274,
     iguali = 275,
     dif = 276,
     diferente = 277,
     Or = 278,
     And = 279,
     IF = 280,
     Else = 281,
     Case = 282,
     Switch = 283,
     For = 284,
     While = 285,
     Break = 286,
     Class = 287,
     draw = 288,
     resize = 289,
     rotate = 290,
     move = 291,
     circunscribe = 292,
     erase = 293,
     comilla = 294,
     p = 295,
     Variables = 296,
     Figures = 297,
     Design = 298,
     Int = 299,
     String = 300,
     Float = 301,
     Arreglo = 302,
     textura = 303,
     blue = 304,
     orange = 305,
     red = 306,
     brownie = 307,
     green = 308,
     yellow = 309,
     white = 310,
     black = 311,
     skyblue = 312,
     grey = 313,
     triangulo = 314,
     dosp = 315,
     New = 316,
     ovalo = 317,
     poligono = 318,
     circulo = 319,
     flecha = 320,
     rectangulo = 321,
     nube = 322,
     punto = 323,
     default = 324,
     texto = 325,
     linea = 326,
     imagen = 327,
     iden = 328,
     Cadena = 329,
     acorch = 330,
     ccorch = 331,
     numerod = 332,
     numero = 333
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 30 "sintactico.y"

//se especifican los tipo de valores para los no terminales y lo terminales
char TEXT [256];
float real;


/* Line 2058 of yacc.c  */
#line 142 "sintactico.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_SINTACTICO_H_INCLUDED  */
