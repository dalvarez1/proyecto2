#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore>
#include <QtGui>
#include <QFileDialog>
#include <QUrl>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //this->setCentralWidget(ui->textEdit);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btprueba_clicked()
{

    QFile archivo("t.txt");
    if(archivo.open(archivo.WriteOnly)){
        QTextStream buffer (&archivo);
        buffer<<ui->textEdit->toPlainText();
    }
    const char* x = "t.txt";
    FILE* salida = fopen(x,"r");
    yyrestart(salida);
    //se crea una tabla se simbolos sin elementos antes del parseo y se pasa como parametro para ser
    //llenada durante el parseo

    tsimbolo *ts = new tsimbolo();

    setTabla(ts);
    yyparse();
    imprime();

}


void MainWindow:: imprime(){
    QLinkedList<QString> temp = getlita();
    int tama = temp.size();
    for(int a =0; a<tama; a++){
        ui->textEdit_2->append(temp.takeFirst());
    }
QLinkedList<tablasimbolos> *e= gettodo();
int numero = e->size();
for(int i=0; i<numero;i++)
{
ui->textEdit_2->append(QString(e->takeFirst().getNom()));
//ui->textEdit_2->append(e->takeFirst().getVar());
/*
Class{
variables{
int a=22;
int b=32;
}
}*/
qDebug()<<e->size();
}
}


void MainWindow::on_actionNUEVO_triggered()
{
bn="";
ui->textEdit->setPlainText("");
}

void MainWindow::on_actionABRIR_triggered()
{
QString od=QFileDialog::getOpenFileName(this,"abrir archivo","*.mcd");
if(!od.isEmpty()){
    QFile arct(od);
    if(arct.open(QFile::ReadOnly | QFile::Text)){
        bn=od;
        QTextStream n(&arct);
        QString text= n.readAll();
        arct.close();
        ui->textEdit->setPlainText(text);

    }else{
        ui->textEdit_2->setPlainText("error de archivo ");
    }

}
}

void MainWindow::on_actionGUARDAR_triggered()
{
QFile arcJ(bn);
if(arcJ.open(QFile::WriteOnly | QFile::Text)){
    QTextStream stream(&arcJ);
    stream<<ui->textEdit->toPlainText();
    arcJ.flush();
    arcJ.close();

}


}

void MainWindow::on_actionGUARDAR_COMO_triggered()
{
QString pbloc=QFileDialog::getSaveFileName(this,"guardar como");
if(!pbloc.isEmpty()){

    bn=pbloc;
    on_actionGUARDAR_triggered();
}


}

void MainWindow::on_actionCOMPILAR_triggered()
{

}

void MainWindow::on_actionEXPORTAR_triggered()
{
/*
    QImage *imagen = new QImage();
    if (imagen-> save ("C:\Users\SAMSUNG\Desktop\p2\p2\p1.jpg"))
         {
           }
           else
           {
           }

*/
}

void MainWindow::on_actionERRORES_triggered()
{

}

void MainWindow::on_actionUSUARIO_triggered()
{

}

void MainWindow::on_actionTECNICO_triggered()
{

}

void MainWindow::on_actionSALIR_triggered()
{

}

void MainWindow::on_actionTABLA_DE_SIMBOLOS_triggered()
{

}

void MainWindow::on_actionTEMAS_AYUDA_triggered()
{

}
