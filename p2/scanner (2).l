%option noyywrap
%{
#include "sintactico.h"
#include <iostream>
#include <QString>
int columna=0;
%}
letra [a-z�A-Z]
comentario "/*"[^'*']*"*/"
digito [0-9]
Cadena letra+
iden ({letra}|"_"|{digito})+
S ("."|":"|"/"|"_"|"\\"|"["|"]")
dir "\""?{letra}({S})*({letra}|{digito}|{S})*"\""?
Numero {digito}+
Numerod {digito}+("."{digito}+)
%%
"print"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return print; }
[+]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mas; }
[-]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menos; }
[*]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return por; }
[/]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return divis; }
[%]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mod; }
[,]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return coma; }
"="             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return igual; }
[;]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return pcoma; }
[(]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return aparen; }
[)]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cparen; }
[<]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menor; }
[>]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mayor; }
">="             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mayori; }
"<="             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menori; }
"=="             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return iguali; }
"!="             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return diferente; }
"||"            { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Or; }
"&&"             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return And; }
"!"             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return dif; }
[}]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cllave; }
[{]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return allave; }
[[]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return acorch; }
[]]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return ccorch; }
"if"        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return IF; }
"else"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Else; }
"switch"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Switch; }
"case"        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Case; }
"for"        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return For; }
"while"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return While; }
"break"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Break; }
"Class"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Class; }
"draw"           { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return draw; }
"resize"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return resize; }
"rotate"         { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return rotate; }
"move"           { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return move; }
"cicunscribe"   { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return circunscribe; }
"erase"          { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return erase; }
"\""             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return comilla; }
[.]             { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return p; }
"variables"     { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Variables; }
 "figures"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Figures; }
  "design"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Design; }
   "int"        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Int; }
 "string"   { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return String; }
 "float"        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Float; }
 "arreglo"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Arreglo; }
{Numero}        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return numero; }
{Numerod}        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return numerod; }
{Cadena}        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return Cadena; }

 "textura"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return textura; }
 "blue"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return blue; }
 "yellow"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return yellow; }
  "orange"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return orange; }
  "red"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return red; }
  "green"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return green; }
  "white"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return white; }
  "black"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return black; }
  "skyblue"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return skyblue; }
  "brownie"     { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return brownie; }
  "grey"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return grey; }
  "triangulo"     { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return triangulo; }
[:]        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return dosp; }
 "ovalo"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return ovalo; }
 "poligono"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return poligono; }
 "circulo"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return circulo; }
 "rectangulo"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return rectangulo; }
 "nube"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return nube; }
 "punto"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return punto; }
 "texto"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return texto; }
 "imagen"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return imagen; }
 "flecha"      { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return flecha; }
 "linea"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return linea; }
 "new"       { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return new; }
{iden}        { columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return iden; }

{comentario}    { /*Se ignoran*/ }
[[:blank:]]     { /*Se ignoran*/ }
.               {std::cout <<yytext<<"Error Lexico "<< std::endl;}
%%

