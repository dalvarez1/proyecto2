#ifndef LISTA_H
#define LISTA_H
#include "tsimbolo.h"

class LISTA
{
public:
    virtual QString todo(tsimbolo *t) const=0;
};

#endif // LISTA_H
