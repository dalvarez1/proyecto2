#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <stdio.h>
#include <stdlib.h>
#include "sintactico.h"
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QLinkedList>
#include <QString>
extern void yyrestart( FILE* archivo);//METODO QUE PASA EL ARCHIVO A FLEX
extern int yyparse(); //METODO QUE INICIA EL ANALISIS SINTACTICO
extern QLinkedList<QString> getlita();

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btprueba_clicked();

private:
    Ui::MainWindow *ui;
    void imprime();
};

#endif // MAINWINDOW_H
