#ifndef AST_EXPRESIONES_H
#define AST_EXPRESIONES_H
#include <QString>


class Ast_expresiones
{
public:
    Ast_expresiones();
    Ast_expresiones*ramaiz;
    Ast_expresiones*ramaderecha;
    QString recorreRamas();
};

#endif // AST_EXPRESIONES_H
