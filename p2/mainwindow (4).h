#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <stdio.h>
#include <stdlib.h>
#include "sintactico.h"
#include "tablasimbolos.h"
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QLinkedList>
#include <QString>
#include <QtGui>
#include <QtCore>
extern void yyrestart( FILE* archivo);//METODO QUE PASA EL ARCHIVO A FLEX
extern int yyparse(); //METODO QUE INICIA EL ANALISIS SINTACTICO
extern QLinkedList<QString> getlita();
extern QLinkedList<tablasimbolos> * gettodo();

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btprueba_clicked();

    void on_actionNUEVO_triggered();

    void on_actionABRIR_triggered();

    void on_actionGUARDAR_triggered();

    void on_actionGUARDAR_COMO_triggered();

    void on_actionCOMPILAR_triggered();

    void on_actionEXPORTAR_triggered();

    void on_actionERRORES_triggered();

    void on_actionUSUARIO_triggered();

    void on_actionTECNICO_triggered();

    void on_actionSALIR_triggered();

    void on_actionTABLA_DE_SIMBOLOS_triggered();

    void on_actionTEMAS_AYUDA_triggered();

private:
    Ui::MainWindow *ui;
    void imprime();
    QString bn;
};

#endif // MAINWINDOW_H
