/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_SINTACTICO_H_INCLUDED
# define YY_YY_SINTACTICO_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mas = 258,
     menos = 259,
     por = 260,
     divis = 261,
     mod = 262,
     coma = 263,
     igual = 264,
     print = 265,
     pcoma = 266,
     aparen = 267,
     cparen = 268,
     cllave = 269,
     allave = 270,
     menor = 271,
     mayor = 272,
     menori = 273,
     mayori = 274,
     iguali = 275,
     dif = 276,
     diferente = 277,
     Or = 278,
     And = 279,
     IF = 280,
     Else = 281,
     Case = 282,
     Switch = 283,
     For = 284,
     While = 285,
     Break = 286,
     color = 287,
     path = 288,
     Class = 289,
     draw = 290,
     resize = 291,
     rotate = 292,
     move = 293,
     circunscribe = 294,
     erase = 295,
     comilla = 296,
     p = 297,
     Variables = 298,
     Figures = 299,
     Design = 300,
     Int = 301,
     String = 302,
     Float = 303,
     Arreglo = 304,
     textura = 305,
     blue = 306,
     orange = 307,
     red = 308,
     brownie = 309,
     green = 310,
     yellow = 311,
     white = 312,
     black = 313,
     skyblue = 314,
     grey = 315,
     triangulo = 316,
     dosp = 317,
     New = 318,
     ovalo = 319,
     poligono = 320,
     circulo = 321,
     Name = 322,
     posx = 323,
     posy = 324,
     Grade = 325,
     Base = 326,
     lineaguion = 327,
     lineapunto = 328,
     lineaguionpunto = 329,
     lineasolida = 330,
     RootFig = 331,
     ChildFig = 332,
     Height = 333,
     flecha = 334,
     rectangulo = 335,
     nube = 336,
     punto = 337,
     default = 338,
     texto = 339,
     linea = 340,
     imagen = 341,
     iden = 342,
     Cadena = 343,
     acorch = 344,
     ccorch = 345,
     numerod = 346,
     numero = 347
   };
#endif
#include "metodoW.h"
#include "LISTA.h"
#include <QLinkedList>


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{


//se especifican los tipo de valores para los no terminales y lo terminales
char TEXT [256];
float real;
char*str;
QLinkedList<LISTA> *LIS;
LISTA *LISTA;



} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_SINTACTICO_H_INCLUDED  */
