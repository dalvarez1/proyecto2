#ifndef TSIMBOLO_H
#define TSIMBOLO_H
#include <QLinkedList>
#include "tablasimbolos.h"
#include <QString>
#include <QDebug>
class tsimbolo : public QLinkedList<tablasimbolos*>
{
public:
    tsimbolo();
    void setValor(QString b, QString id);
    void insertar(tablasimbolos*s, QString id);
    QString buscar(QString id);
};

#endif // TSIMBOLO_H
