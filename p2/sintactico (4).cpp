/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */


#include "scanner.h"//se importa el header del analisis sintactico

#include <iostream> //libreria para imprimir en cosola de C
#include "mainwindow.h"
#include <QString> //libreria para manejo de STRINGS de QT
#include <tsimbolo.h>
#include <QHash> //Libreria para manejar HASH TABLES de QT, se usa para la tabla de simbolos
#include <QDebug>
#include <QTextEdit> //libreria QTextEdit de QT para poder mostrar el resultado en pantalla
#include <QLinkedList>
#include "tablasimbolos.h" //clase que contiene los valores de la tabla de simbolos

extern int yylineno; //linea actual donde se encuentra el parser (analisis lexico) lo maneja BISON
extern int columna; //columna actual donde se encuentra el parser (analisis lexico) lo maneja BISON
extern char *yytext; //lexema actual donde esta el parser (analisis lexico) lo maneja BISON

QLinkedList<QString> lista;
QHash <QString, QString> tabla_s; //TABLA DE SIMBOLOS


QLinkedList<tablasimbolos> *tablasimbolo=new QLinkedList<tablasimbolos>();
tablasimbolos *s;
	tsimbolo * t=new tsimbolo();
QLinkedList<tablasimbolos> *gettodo(){
return tablasimbolo;
}

int yyerror(const char* mens){
//metodo que se llama al haber un error sintactico
//SE IMPRIME EN CONSOLA EL ERROR
std::cout <<mens<<" "<<yytext<< std::endl;
return 0;
}

QLinkedList<QString> getlita(){
return lista;
}




# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sintactico.h".  */
#ifndef YY_YY_SINTACTICO_H_INCLUDED
# define YY_YY_SINTACTICO_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mas = 258,
     menos = 259,
     por = 260,
     divis = 261,
     mod = 262,
     coma = 263,
     igual = 264,
     print = 265,
     pcoma = 266,
     aparen = 267,
     cparen = 268,
     cllave = 269,
     allave = 270,
     menor = 271,
     mayor = 272,
     menori = 273,
     mayori = 274,
     iguali = 275,
     dif = 276,
     diferente = 277,
     Or = 278,
     And = 279,
     IF = 280,
     Else = 281,
     Case = 282,
     Switch = 283,
     For = 284,
     While = 285,
     Break = 286,
     Class = 287,
     draw = 288,
     resize = 289,
     rotate = 290,
     move = 291,
     circunscribe = 292,
     erase = 293,
     comilla = 294,
     p = 295,
     Variables = 296,
     Figures = 297,
     Design = 298,
     Int = 299,
     String = 300,
     Float = 301,
     Arreglo = 302,
     textura = 303,
     blue = 304,
     orange = 305,
     red = 306,
     brownie = 307,
     green = 308,
     yellow = 309,
     white = 310,
     black = 311,
     skyblue = 312,
     grey = 313,
     triangulo = 314,
     dosp = 315,
     New = 316,
     ovalo = 317,
     poligono = 318,
     circulo = 319,
     Name = 320,
     posx = 321,
     posy = 322,
     Grade = 323,
     Base = 324,
     RootFig = 325,
     ChildFig = 326,
     Height = 327,
     flecha = 328,
     rectangulo = 329,
     nube = 330,
     punto = 331,
     default = 332,
     texto = 333,
     linea = 334,
     imagen = 335,
     iden = 336,
     Cadena = 337,
     acorch = 338,
     ccorch = 339,
     numerod = 340,
     numero = 341
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{


//se especifican los tipo de valores para los no terminales y lo terminales
char TEXT [256];
float real;



} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_SINTACTICO_H_INCLUDED  */

/* Copy the second part of user declarations.  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   835

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  87
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  39
/* YYNRULES -- Number of rules.  */
#define YYNRULES  162
/* YYNRULES -- Number of states.  */
#define YYNSTATES  452

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   341

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     9,    14,    19,    24,    29,    35,    41,
      47,    52,    57,    62,    65,    67,    75,    84,    93,   105,
     116,   118,   130,   138,   140,   143,   145,   153,   165,   180,
     192,   204,   210,   213,   216,   219,   222,   225,   228,   231,
     234,   237,   240,   242,   244,   246,   248,   250,   252,   254,
     256,   258,   260,   268,   276,   284,   292,   300,   308,   316,
     324,   332,   340,   350,   359,   363,   367,   377,   386,   391,
     396,   401,   406,   412,   417,   422,   427,   432,   435,   441,
     445,   447,   448,   450,   452,   454,   456,   458,   461,   463,
     465,   469,   470,   473,   477,   481,   485,   489,   493,   497,
     499,   501,   505,   509,   511,   514,   517,   519,   521,   526,
     531,   537,   543,   545,   554,   558,   562,   566,   570,   576,
     582,   588,   592,   594,   596,   600,   602,   604,   608,   610,
     612,   618,   622,   626,   632,   634,   640,   644,   648,   650,
     656,   670,   682,   686,   690,   694,   700,   702,   708,   712,
     716,   718,   722,   726,   728,   732,   736,   740,   742,   744,
     748,   751,   753
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      88,     0,    -1,    32,    81,    15,    89,    14,    -1,    32,
      15,    89,    14,    -1,    41,    15,    14,    89,    -1,    42,
      15,    14,    89,    -1,    43,    15,    14,    89,    -1,    41,
      15,   111,    14,    89,    -1,    42,    15,    93,    14,    89,
      -1,    43,    15,    90,    14,    89,    -1,    41,    15,   111,
      14,    -1,    42,    15,    93,    14,    -1,    43,    15,    90,
      14,    -1,    90,    91,    -1,    91,    -1,    25,    12,   107,
      13,    15,    90,    14,    -1,    25,    12,   107,    13,    15,
      90,    14,   106,    -1,    25,    12,   107,    13,    15,    90,
      14,   106,    -1,    25,    12,   107,    13,    15,    90,    14,
      26,    15,    90,    14,    -1,    28,    12,    81,    13,    15,
     104,    77,    60,    90,    14,    -1,   123,    -1,    29,    12,
     111,    11,   107,    11,   105,    13,    15,    90,    14,    -1,
      30,    12,   107,    13,    15,    90,    14,    -1,   111,    -1,
      92,    91,    -1,    92,    -1,    38,    12,    65,    60,    81,
      13,    11,    -1,    37,    12,    70,    60,    81,     8,    71,
      60,    81,    13,    11,    -1,    36,    12,    65,    60,    81,
       8,    67,    60,   109,     8,    66,   109,    13,    11,    -1,
      35,    12,    65,    60,    81,     8,    68,    60,   109,    13,
      11,    -1,    34,    12,    69,    60,   109,     8,    72,    60,
     109,    13,    11,    -1,    33,    12,    81,    13,    11,    -1,
      94,    93,    -1,    95,    93,    -1,    96,    93,    -1,    97,
      93,    -1,    98,    93,    -1,    99,    93,    -1,   100,    93,
      -1,   101,    93,    -1,   102,    93,    -1,   103,    93,    -1,
      94,    -1,    95,    -1,    96,    -1,    97,    -1,    98,    -1,
      99,    -1,   100,    -1,   101,    -1,   102,    -1,   103,    -1,
      59,     9,    61,    59,    12,    13,    11,    -1,    62,     9,
      61,    62,    12,    13,    11,    -1,    75,     9,    61,    75,
      12,    13,    11,    -1,    64,     9,    61,    64,    12,    13,
      11,    -1,    78,     9,    61,    78,    12,    13,    11,    -1,
      80,     9,    61,    80,    12,    13,    11,    -1,    73,     9,
      61,    73,    12,    13,    11,    -1,    79,     9,    61,    79,
      12,    13,    11,    -1,    63,     9,    61,    63,    12,    13,
      11,    -1,    74,     9,    61,    74,    12,    13,    11,    -1,
      27,    81,    60,    15,    90,    31,    11,    14,   104,    -1,
      27,    81,    60,    15,    90,    31,    11,    14,    -1,    81,
       3,     3,    -1,    81,     4,     4,    -1,    26,    25,    12,
     107,    13,    15,    90,    14,   106,    -1,    26,    25,    12,
     107,    13,    15,    90,    14,    -1,    26,    15,    90,    14,
      -1,   109,    19,   109,   107,    -1,   109,    18,   109,   107,
      -1,   109,    20,   109,   107,    -1,   109,    21,     9,   109,
     107,    -1,   109,    16,   109,   107,    -1,   109,    17,   109,
     107,    -1,   109,    23,   109,   107,    -1,   109,    24,   109,
     107,    -1,   108,   107,    -1,    12,   107,    13,   108,   107,
      -1,    12,   107,    13,    -1,    81,    -1,    -1,    19,    -1,
      18,    -1,    17,    -1,    16,    -1,    20,    -1,    21,     9,
      -1,    23,    -1,    24,    -1,    12,   108,    13,    -1,    -1,
       4,   109,    -1,   109,     3,   109,    -1,   109,     4,   109,
      -1,   109,     5,   109,    -1,   109,     6,   109,    -1,   109,
       7,   109,    -1,    12,   109,    13,    -1,    86,    -1,    81,
      -1,   110,     3,   110,    -1,    39,    81,    39,    -1,    81,
      -1,   113,   111,    -1,   114,   111,    -1,   113,    -1,   114,
      -1,    81,     9,   123,    11,    -1,    81,     9,   110,    11,
      -1,    81,     9,   123,    11,   111,    -1,    81,     9,   110,
      11,   111,    -1,   112,    -1,    48,    81,     9,    61,    48,
      12,    13,    11,    -1,    44,   115,    11,    -1,    45,   116,
      11,    -1,    46,   117,    11,    -1,    81,     9,   123,    -1,
      44,    81,     9,    86,    11,    -1,    45,    81,     9,   110,
      11,    -1,    46,    81,     9,    85,    11,    -1,    81,     8,
     115,    -1,    81,    -1,   118,    -1,    81,     8,   116,    -1,
      81,    -1,   119,    -1,    81,     8,   117,    -1,    81,    -1,
     120,    -1,    81,     9,    86,     8,   118,    -1,    81,     8,
     118,    -1,    81,     9,   123,    -1,    81,     9,   123,     8,
     118,    -1,    81,    -1,    81,     9,   110,     8,   119,    -1,
      81,     8,   119,    -1,    81,     9,   110,    -1,    81,    -1,
      81,     9,    85,     8,   120,    -1,    81,    83,   123,    84,
      83,   123,    84,     9,    83,   121,    84,     8,   120,    -1,
      81,    83,   123,    84,    83,   123,    84,     9,    83,   121,
      84,    -1,    81,     9,    85,    -1,    81,     8,   120,    -1,
      81,     9,   123,    -1,    81,     9,   123,     8,   120,    -1,
      81,    -1,    12,   122,    13,     8,   121,    -1,    12,   122,
      13,    -1,    86,     8,   122,    -1,    86,    -1,   123,     3,
     124,    -1,   123,     4,   124,    -1,   124,    -1,   124,     5,
     125,    -1,   124,     6,   125,    -1,   124,     7,   125,    -1,
     125,    -1,    86,    -1,    12,   123,    13,    -1,     4,   123,
      -1,    85,    -1,    81,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   186,   186,   187,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   199,   200,   202,   203,   204,   205,   206,
     207,   208,   209,   210,   211,   212,   216,   217,   218,   219,
     220,   221,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,   234,   235,   236,   237,   238,   239,   240,   241,
     242,   243,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   264,   265,   268,   268,   270,   271,   272,   275,
     276,   277,   278,   279,   280,   281,   282,   283,   284,   285,
     286,   287,   291,   292,   293,   294,   295,   296,   297,   298,
     299,   300,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   316,   317,   318,   320,   321,   322,   323,   324,   325,
     326,   327,   328,   331,   333,   334,   335,   336,   339,   343,
     344,   346,   347,   348,   350,   351,   352,   354,   355,   356,
     358,   359,   360,   361,   362,   364,   365,   366,   367,   369,
     370,   371,   372,   373,   374,   375,   376,   378,   379,   382,
     383,   386,   390,   395,   403,   404,   405,   406,   410,   415,
     416,   417,   422
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "mas", "menos", "por", "divis", "mod",
  "coma", "igual", "print", "pcoma", "aparen", "cparen", "cllave",
  "allave", "menor", "mayor", "menori", "mayori", "iguali", "dif",
  "diferente", "Or", "And", "IF", "Else", "Case", "Switch", "For", "While",
  "Break", "Class", "draw", "resize", "rotate", "move", "circunscribe",
  "erase", "comilla", "p", "Variables", "Figures", "Design", "Int",
  "String", "Float", "Arreglo", "textura", "blue", "orange", "red",
  "brownie", "green", "yellow", "white", "black", "skyblue", "grey",
  "triangulo", "dosp", "New", "ovalo", "poligono", "circulo", "Name",
  "posx", "posy", "Grade", "Base", "RootFig", "ChildFig", "Height",
  "flecha", "rectangulo", "nube", "punto", "default", "texto", "linea",
  "imagen", "iden", "Cadena", "acorch", "ccorch", "numerod", "numero",
  "$accept", "INICIO", "CUERPO", "INSTRUCCIONES", "INSTRUCCION",
  "INSTRUCCIONF", "FIG", "TRIANGULO", "OVALO", "NUBE", "CIRCULO", "TEXTO",
  "IMAGEN", "FLECHA", "LINEA", "POLIGONO", "RECTANGULO", "CASOS",
  "INCREMENTO", "ELSI", "EXPRESIONLOGICA", "EXPRE", "EXPRESIONNUMERICA",
  "EXPRESIONCADENA", "ASIGNACION", "TEXTURA", "DECLARACION", "ASIGNAR",
  "ASIGM", "ASIGM1", "ASIGM2", "VALORES", "VALORES1", "VALORES2",
  "ARREGLO", "A", "E", "T", "F", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    87,    88,    88,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    90,    90,    91,    91,    91,    91,    91,
      91,    91,    91,    91,    91,    91,    92,    92,    92,    92,
      92,    92,    93,    93,    93,    93,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      93,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   104,   105,   105,   106,   106,   106,   107,
     107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
     107,   107,   108,   108,   108,   108,   108,   108,   108,   108,
     108,   108,   109,   109,   109,   109,   109,   109,   109,   109,
     109,   110,   110,   110,   111,   111,   111,   111,   111,   111,
     111,   111,   111,   112,   113,   113,   113,   113,   114,   114,
     114,   115,   115,   115,   116,   116,   116,   117,   117,   117,
     118,   118,   118,   118,   118,   119,   119,   119,   119,   120,
     120,   120,   120,   120,   120,   120,   120,   121,   121,   122,
     122,   123,   123,   123,   124,   124,   124,   124,   125,   125,
     125,   125,   125
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     5,     4,     4,     4,     4,     5,     5,     5,
       4,     4,     4,     2,     1,     7,     8,     8,    11,    10,
       1,    11,     7,     1,     2,     1,     7,    11,    14,    11,
      11,     5,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     7,     7,     7,     7,     7,     7,     7,     7,
       7,     7,     9,     8,     3,     3,     9,     8,     4,     4,
       4,     4,     5,     4,     4,     4,     4,     2,     5,     3,
       1,     0,     1,     1,     1,     1,     1,     2,     1,     1,
       3,     0,     2,     3,     3,     3,     3,     3,     3,     1,
       1,     3,     3,     1,     2,     2,     1,     1,     4,     4,
       5,     5,     1,     8,     3,     3,     3,     3,     5,     5,
       5,     3,     1,     1,     3,     1,     1,     3,     1,     1,
       5,     3,     3,     5,     1,     5,     3,     3,     1,     5,
      13,    11,     3,     3,     3,     5,     1,     5,     3,     3,
       1,     3,     3,     1,     3,     3,     3,     1,     1,     3,
       2,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     0,     1,     0,     0,     0,     0,
       0,     0,     0,     0,     3,     0,     0,     0,     0,     0,
       0,     0,     0,   112,   106,   107,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   162,   161,   158,     0,    14,    25,    23,    20,   153,
     157,     2,     4,   122,     0,   123,   125,     0,   126,   128,
       0,   129,     0,     0,    10,   104,   105,     5,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    11,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,   162,
     160,     0,     6,    81,     0,     0,    81,     0,     0,     0,
       0,     0,     0,    12,    13,    24,     0,     0,     0,     0,
       0,     0,     0,   114,     0,     0,   115,     0,     0,     0,
     116,     0,     0,   162,     0,   117,     7,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     8,   159,     0,
      81,    85,    84,    83,    82,    86,     0,    88,    89,   100,
      99,     0,    81,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     9,   151,   152,   154,   155,   156,   122,
     121,   123,   158,   132,   125,   124,   126,   103,     0,   128,
     127,   129,   161,   144,     0,     0,     0,     0,   109,   108,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   100,    92,     0,     0,     0,    87,     0,    77,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    81,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   118,     0,     0,     0,   119,     0,     0,
     120,     0,     0,     0,   102,   101,   111,   110,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    91,
      90,    98,     0,    93,    94,    95,    96,    97,    81,    81,
      81,    81,    81,     0,    81,    81,     0,     0,     0,    31,
       0,     0,     0,     0,     0,   158,   134,   130,   133,   137,
     138,   135,   161,   146,   139,   145,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    91,    81,
       0,     0,    73,    74,    70,    69,    71,    81,    75,    76,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    52,    53,    60,    55,    58,    61,
      54,    56,    59,    57,     0,    78,    15,    92,    72,     0,
       0,     0,     0,    22,     0,     0,     0,     0,    26,   131,
     136,   143,     0,   113,     0,    16,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      64,    65,     0,     0,     0,     0,     0,     0,     0,    81,
       0,    19,     0,     0,     0,     0,     0,     0,     0,    18,
       0,     0,    21,    30,    29,     0,    27,   150,     0,   141,
       0,     0,     0,     0,   148,     0,     0,    63,     0,   149,
       0,   140,     0,    62,    28,   147,    67,     0,    66,     0,
       0,    68
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     9,    64,    65,    66,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,   341,   372,   385,
     228,   172,   173,   144,    67,    23,    24,    25,    74,    77,
      80,    75,    78,    81,   418,   428,    68,    69,    70
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -261
static const yytype_int16 yypact[] =
{
     -11,   -10,    26,   181,    35,  -261,    44,    51,    57,    69,
     181,   127,   678,   202,  -261,    79,   181,    56,    66,   106,
     116,   210,   211,  -261,   -21,   -21,   181,   259,   271,   273,
     281,   286,   292,   299,   300,   315,   318,   320,   745,   745,
     745,   745,   745,   745,   745,   745,   745,   745,    84,    84,
     181,   249,   317,   324,   330,   333,   337,   338,   340,   342,
     343,   210,  -261,  -261,   277,  -261,   653,  -261,    29,   268,
    -261,  -261,  -261,   111,   321,  -261,   122,   345,  -261,    33,
     346,  -261,   350,    82,   181,  -261,  -261,  -261,   304,   305,
     309,   319,   327,   328,   329,   332,   353,   356,   181,  -261,
    -261,  -261,  -261,  -261,  -261,  -261,  -261,  -261,  -261,  -261,
    -261,    54,  -261,   698,   280,   -21,   698,   301,   291,   326,
     354,   348,   357,   181,  -261,  -261,    84,    84,    84,    84,
      84,   347,   148,  -261,   358,   -28,  -261,   366,   184,    84,
    -261,   360,   368,    34,   163,    86,  -261,   335,   336,   363,
     359,   364,   325,   352,   351,   371,   378,  -261,  -261,    32,
     698,  -261,  -261,  -261,  -261,  -261,   457,  -261,  -261,   194,
    -261,   455,   698,   782,   456,   462,   463,   464,   415,   419,
     420,   424,   425,  -261,   268,   268,  -261,  -261,  -261,   169,
    -261,  -261,    41,    65,   191,  -261,  -261,  -261,   137,    39,
    -261,  -261,    76,   209,    11,   444,   454,   -28,   -21,   -21,
     482,   483,   484,   488,   490,   491,   492,   493,   494,   496,
      32,  -261,   279,   497,   509,   757,  -261,   499,  -261,    32,
      32,    32,    32,    32,    32,    32,    32,    32,    32,   500,
      32,    32,   502,   698,   503,   504,    32,   439,   442,   450,
     470,   186,   472,  -261,   472,   -28,   474,  -261,   245,   478,
    -261,   478,   436,   523,  -261,  -261,  -261,  -261,   548,   549,
     550,   551,   553,   554,   558,   559,   566,   567,   188,   138,
    -261,  -261,   653,   279,   279,   203,   313,   313,    58,    58,
      58,    58,    58,    32,    58,    58,   555,   525,   653,  -261,
     428,   573,   575,   579,   576,   583,   269,  -261,  -261,     9,
     289,  -261,   135,    46,  -261,  -261,    84,   581,   585,   588,
     589,   590,   591,   593,   594,   595,   596,   597,   708,   698,
     339,    32,  -261,  -261,  -261,  -261,  -261,    58,  -261,  -261,
     511,   532,   529,   367,   542,   544,   552,   545,   604,   472,
     474,   478,    31,   606,  -261,  -261,  -261,  -261,  -261,  -261,
    -261,  -261,  -261,  -261,   605,  -261,   613,   247,  -261,   560,
     562,   252,   610,  -261,   571,   582,   586,   592,  -261,  -261,
    -261,  -261,   632,  -261,     3,  -261,   633,   653,   646,   649,
     635,    32,    32,    32,   577,   584,   653,   654,   653,   426,
    -261,  -261,   653,   238,   334,   438,   655,   657,   453,   698,
     512,  -261,   540,   662,   664,   611,   665,   607,   612,  -261,
     666,   683,  -261,  -261,  -261,    32,  -261,   687,   690,   692,
     689,   691,   403,   607,   700,   478,   653,   555,   695,  -261,
     657,  -261,   599,  -261,  -261,  -261,   697,    14,  -261,   653,
     626,  -261
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -261,  -261,    30,  -260,   -64,  -261,   788,  -261,  -261,  -261,
    -261,  -261,  -261,  -261,  -261,  -261,  -261,   272,  -261,   267,
    -109,  -144,  -129,  -122,    -5,  -261,  -261,  -261,   602,   601,
     608,  -128,  -124,  -136,   290,   303,   -40,   177,   164
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -143
static const yytype_int16 yytable[] =
{
     124,   201,   125,   191,   171,     3,    22,   176,   110,   111,
     196,   142,   207,   198,   126,   127,   224,   256,   396,    85,
      86,     1,   330,    17,    18,    19,     5,    20,   397,   449,
     222,   225,   126,   127,   126,   127,   159,  -103,   343,   397,
      15,   137,   138,   145,   220,  -103,    72,   137,   258,   252,
      10,   223,   253,   197,   351,   258,    87,   126,   127,    11,
      21,   229,   331,   231,   232,   233,    12,   158,   126,   127,
     160,     4,    13,   254,   161,   162,   163,   164,   165,   166,
     112,   167,   168,    14,   259,   265,    48,   260,    48,   126,
     127,   278,   193,    71,    49,   262,    49,   209,   203,   204,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     175,   294,   295,   221,   146,   382,   139,   300,   170,   131,
     132,   142,   139,   314,   307,   315,   308,   399,   157,   139,
     134,   135,   311,   309,   297,   329,   408,    73,   410,   169,
     207,    16,   412,   259,   170,   256,  -142,    76,   257,   -79,
     328,   -79,    48,   183,   161,   162,   163,   164,   165,   166,
      49,   167,   168,   143,   337,   109,   207,    62,    63,    62,
      63,    17,    18,    19,   208,    20,   442,   131,   251,   332,
     333,   334,   335,   336,   364,   338,   339,    79,    48,   450,
      48,   229,   230,   231,   232,   233,    49,    82,    49,   134,
     255,   281,   367,   266,   267,   -80,    48,   -80,    21,   232,
     233,   193,   126,   127,    49,   381,    50,   261,   203,    83,
     365,   379,     6,     7,     8,    84,   380,    51,   368,   109,
      52,    53,    54,    62,   192,    55,    56,    57,    58,    59,
      60,   229,   230,   231,   232,   233,    17,    18,    19,    48,
      20,   413,   231,   232,   233,   388,   389,    49,   -94,   -94,
     -94,   113,   403,   404,   405,   109,   124,   109,    88,   202,
      63,    62,   305,   128,   129,   130,   352,   349,   251,   124,
      89,    48,    90,    61,   231,   232,   233,    62,    63,    49,
      91,   123,   186,   187,   188,    92,   432,   350,   255,   441,
     420,    93,    51,   184,   185,    52,    53,    54,    94,    95,
      55,    56,    57,    58,    59,    60,   229,   230,   231,   232,
     233,    17,    18,    19,    96,    20,   109,    97,   -94,   114,
     312,    63,   133,   -94,    98,   124,   115,   229,   230,   231,
     232,   233,   116,    48,   124,   117,   124,   414,   124,   118,
     119,    49,   120,   366,   121,   122,   136,   140,    61,   141,
     178,   174,    62,    63,    51,   147,   148,    52,    53,    54,
     149,    48,    55,    56,    57,    58,    59,    60,   124,    49,
     150,   373,   177,    17,    18,    19,   124,    20,   151,   152,
     153,   179,    51,   154,   210,    52,    53,    54,   211,   215,
      55,    56,    57,    58,    59,    60,   229,   230,   231,   232,
     233,    17,    18,    19,   155,    20,   438,   156,   181,   180,
      61,   205,   182,   213,    62,    63,   212,   216,   189,   217,
      48,   229,   230,   231,   232,   233,   344,   214,    49,   194,
     411,   229,   230,   231,   232,   233,   415,   199,    61,   206,
     218,    51,    62,    63,    52,    53,    54,    48,   219,    55,
      56,    57,    58,    59,    60,    49,   226,   419,   227,   242,
      17,    18,    19,   243,    20,   246,   244,   245,    51,   247,
     248,    52,    53,    54,   249,   250,    55,    56,    57,    58,
      59,    60,   263,   264,   268,   269,   270,    17,    18,    19,
     271,    20,   272,   273,   274,   275,   276,    61,   277,   293,
     279,    62,    63,   159,   282,   299,    48,   296,   298,   316,
     301,   160,   280,   302,    49,   161,   162,   163,   164,   165,
     166,   303,   167,   168,    61,   317,   342,    51,    62,    63,
      52,    53,    54,   421,    48,    55,    56,    57,    58,    59,
      60,   304,    49,   306,   422,   310,    17,    18,    19,   313,
      20,   318,   319,   320,   321,    51,   322,   323,    52,    53,
      54,   324,   325,    55,    56,    57,    58,    59,    60,   326,
     327,   345,   340,   346,    17,    18,    19,   347,    20,   348,
     169,   252,   369,    61,   353,   170,   354,    62,    63,   355,
     356,   357,   358,    48,   359,   360,   361,   362,   363,   370,
     371,    49,   375,   446,   374,   378,   377,   383,   280,   376,
     386,    61,   387,   390,    51,    62,    63,    52,    53,    54,
      48,   391,    55,    56,    57,    58,    59,    60,    49,   384,
     451,   395,   392,    17,    18,    19,   393,    20,   398,   400,
     402,    51,   394,   401,    52,    53,    54,    48,   406,    55,
      56,    57,    58,    59,    60,    49,   409,   407,   416,   417,
      17,    18,    19,   423,    20,   424,   426,   425,    51,   430,
      61,    52,    53,    54,    62,    63,    55,    56,    57,    58,
      59,    60,    26,   427,   431,   433,   429,    17,    18,    19,
     435,    20,   159,   434,   436,   437,   444,    61,   440,   443,
     160,    62,    63,   448,   161,   162,   163,   164,   165,   166,
     328,   167,   168,   447,   161,   162,   163,   164,   165,   166,
     445,   167,   168,   190,    61,   195,   439,    27,    62,    63,
      28,    29,    30,     0,     0,   200,     0,     0,     0,     0,
       0,    31,    32,    33,     0,     0,    34,    35,    36,     0,
     229,   230,   231,   232,   233,     0,     0,     0,     0,     0,
     281,     0,     0,   234,   235,   236,   237,   238,   239,   169,
     240,   241,     0,     0,   170,   229,   230,   231,   232,   233,
       0,     0,     0,     0,     0,     0,     0,     0,   234,   235,
     236,   237,   238,   239,    27,   240,   241,    28,    29,    30,
       0,     0,     0,     0,     0,     0,     0,     0,    31,    32,
      33,     0,     0,    34,    35,    36,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-261)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      64,   137,    66,   131,   113,    15,    11,   116,    48,    49,
     134,    39,     3,   135,     3,     4,   160,     8,    15,    24,
      25,    32,   282,    44,    45,    46,     0,    48,    25,    15,
     159,   160,     3,     4,     3,     4,     4,     3,   298,    25,
      10,     8,     9,    83,    12,    11,    16,     8,     9,     8,
      15,   160,    11,    81,     8,     9,    26,     3,     4,    15,
      81,     3,     4,     5,     6,     7,    15,    13,     3,     4,
      12,    81,    15,     8,    16,    17,    18,    19,    20,    21,
      50,    23,    24,    14,     8,   207,     4,    11,     4,     3,
       4,   220,   132,    14,    12,    84,    12,    11,   138,   139,
     229,   230,   231,   232,   233,   234,   235,   236,   237,   238,
     115,   240,   241,    81,    84,    84,    83,   246,    86,     8,
       9,    39,    83,   259,   252,   261,   254,   387,    98,    83,
       8,     9,   256,   255,   243,   279,   396,    81,   398,    81,
       3,    14,   402,     8,    86,     8,    11,    81,    11,    11,
      12,    13,     4,   123,    16,    17,    18,    19,    20,    21,
      12,    23,    24,    81,   293,    81,     3,    85,    86,    85,
      86,    44,    45,    46,    11,    48,   436,     8,     9,   288,
     289,   290,   291,   292,   328,   294,   295,    81,     4,   449,
       4,     3,     4,     5,     6,     7,    12,    81,    12,     8,
       9,    13,   331,   208,   209,    11,     4,    13,    81,     6,
       7,   251,     3,     4,    12,   351,    14,     8,   258,     9,
     329,   349,    41,    42,    43,    14,   350,    25,   337,    81,
      28,    29,    30,    85,    86,    33,    34,    35,    36,    37,
      38,     3,     4,     5,     6,     7,    44,    45,    46,     4,
      48,    13,     5,     6,     7,     3,     4,    12,    11,    12,
      13,    12,   391,   392,   393,    81,   330,    81,     9,    85,
      86,    85,    86,     5,     6,     7,   316,     8,     9,   343,
       9,     4,     9,    81,     5,     6,     7,    85,    86,    12,
       9,    14,   128,   129,   130,     9,   425,     8,     9,   435,
     409,     9,    25,   126,   127,    28,    29,    30,     9,     9,
      33,    34,    35,    36,    37,    38,     3,     4,     5,     6,
       7,    44,    45,    46,     9,    48,    81,     9,    81,    12,
      85,    86,    11,    86,    14,   399,    12,     3,     4,     5,
       6,     7,    12,     4,   408,    12,   410,    13,   412,    12,
      12,    12,    12,    14,    12,    12,    11,    11,    81,     9,
      69,    81,    85,    86,    25,    61,    61,    28,    29,    30,
      61,     4,    33,    34,    35,    36,    37,    38,   442,    12,
      61,    14,    81,    44,    45,    46,   450,    48,    61,    61,
      61,    65,    25,    61,    59,    28,    29,    30,    62,    74,
      33,    34,    35,    36,    37,    38,     3,     4,     5,     6,
       7,    44,    45,    46,    61,    48,    13,    61,    70,    65,
      81,    61,    65,    64,    85,    86,    63,    75,    81,    78,
       4,     3,     4,     5,     6,     7,     8,    73,    12,    81,
      14,     3,     4,     5,     6,     7,     8,    81,    81,    81,
      79,    25,    85,    86,    28,    29,    30,     4,    80,    33,
      34,    35,    36,    37,    38,    12,     9,    14,    13,    13,
      44,    45,    46,    11,    48,    60,    13,    13,    25,    60,
      60,    28,    29,    30,    60,    60,    33,    34,    35,    36,
      37,    38,    48,    39,    12,    12,    12,    44,    45,    46,
      12,    48,    12,    12,    12,    12,    12,    81,    12,     9,
      13,    85,    86,     4,    15,    11,     4,    15,    15,    83,
      81,    12,    13,    81,    12,    16,    17,    18,    19,    20,
      21,    81,    23,    24,    81,    12,    11,    25,    85,    86,
      28,    29,    30,    31,     4,    33,    34,    35,    36,    37,
      38,    81,    12,    81,    14,    81,    44,    45,    46,    81,
      48,    13,    13,    13,    13,    25,    13,    13,    28,    29,
      30,    13,    13,    33,    34,    35,    36,    37,    38,    13,
      13,     8,    27,     8,    44,    45,    46,     8,    48,    13,
      81,     8,    81,    81,    13,    86,    11,    85,    86,    11,
      11,    11,    11,     4,    11,    11,    11,    11,    11,    77,
      81,    12,    68,    14,    72,    11,    71,    11,    13,    67,
      60,    81,    60,    13,    25,    85,    86,    28,    29,    30,
       4,    60,    33,    34,    35,    36,    37,    38,    12,    26,
      14,     9,    60,    44,    45,    46,    60,    48,    15,     3,
      15,    25,    60,     4,    28,    29,    30,     4,    81,    33,
      34,    35,    36,    37,    38,    12,    12,    83,    13,    12,
      44,    45,    46,    11,    48,    11,    11,    66,    25,    13,
      81,    28,    29,    30,    85,    86,    33,    34,    35,    36,
      37,    38,    14,    86,    11,     8,    84,    44,    45,    46,
       8,    48,     4,    13,    15,    14,    11,    81,     8,   437,
      12,    85,    86,   446,    16,    17,    18,    19,    20,    21,
      12,    23,    24,    26,    16,    17,    18,    19,    20,    21,
     440,    23,    24,   131,    81,   134,   433,    59,    85,    86,
      62,    63,    64,    -1,    -1,   137,    -1,    -1,    -1,    -1,
      -1,    73,    74,    75,    -1,    -1,    78,    79,    80,    -1,
       3,     4,     5,     6,     7,    -1,    -1,    -1,    -1,    -1,
      13,    -1,    -1,    16,    17,    18,    19,    20,    21,    81,
      23,    24,    -1,    -1,    86,     3,     4,     5,     6,     7,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    16,    17,
      18,    19,    20,    21,    59,    23,    24,    62,    63,    64,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    73,    74,
      75,    -1,    -1,    78,    79,    80,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    32,    88,    15,    81,     0,    41,    42,    43,    89,
      15,    15,    15,    15,    14,    89,    14,    44,    45,    46,
      48,    81,   111,   112,   113,   114,    14,    59,    62,    63,
      64,    73,    74,    75,    78,    79,    80,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,     4,    12,
      14,    25,    28,    29,    30,    33,    34,    35,    36,    37,
      38,    81,    85,    86,    90,    91,    92,   111,   123,   124,
     125,    14,    89,    81,   115,   118,    81,   116,   119,    81,
     117,   120,    81,     9,    14,   111,   111,    89,     9,     9,
       9,     9,     9,     9,     9,     9,     9,     9,    14,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    81,
     123,   123,    89,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    14,    91,    91,     3,     4,     5,     6,
       7,     8,     9,    11,     8,     9,    11,     8,     9,    83,
      11,     9,    39,    81,   110,   123,    89,    61,    61,    61,
      61,    61,    61,    61,    61,    61,    61,    89,    13,     4,
      12,    16,    17,    18,    19,    20,    21,    23,    24,    81,
      86,   107,   108,   109,    81,   111,   107,    81,    69,    65,
      65,    70,    65,    89,   124,   124,   125,   125,   125,    81,
     115,   118,    86,   123,    81,   116,   119,    81,   110,    81,
     117,   120,    85,   123,   123,    61,    81,     3,    11,    11,
      59,    62,    63,    64,    73,    74,    75,    78,    79,    80,
      12,    81,   109,   107,   108,   109,     9,    13,   107,     3,
       4,     5,     6,     7,    16,    17,    18,    19,    20,    21,
      23,    24,    13,    11,    13,    13,    60,    60,    60,    60,
      60,     9,     8,    11,     8,     9,     8,    11,     9,     8,
      11,     8,    84,    48,    39,   110,   111,   111,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,   109,    13,
      13,    13,    15,   109,   109,   109,   109,   109,   109,   109,
     109,   109,   109,     9,   109,   109,    15,   107,    15,    11,
     109,    81,    81,    81,    81,    86,    81,   118,   118,   110,
      81,   119,    85,    81,   120,   120,    83,    12,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    12,   108,
      90,     4,   107,   107,   107,   107,   107,   109,   107,   107,
      27,   104,    11,    90,     8,     8,     8,     8,    13,     8,
       8,     8,   123,    13,    11,    11,    11,    11,    11,    11,
      11,    11,    11,    11,   108,   107,    14,   109,   107,    81,
      77,    81,   105,    14,    72,    68,    67,    71,    11,   118,
     119,   120,    84,    11,    26,   106,    60,    60,     3,     4,
      13,    60,    60,    60,    60,     9,    15,    25,    15,    90,
       3,     4,    15,   109,   109,   109,    81,    83,    90,    12,
      90,    14,    90,    13,    13,     8,    13,    12,   121,    14,
     107,    31,    14,    11,    11,    66,    11,    86,   122,    84,
      13,    11,   109,     8,    13,     8,    15,    14,    13,   122,
       8,   120,    90,   104,    11,   121,    14,    26,   106,    15,
      90,    14
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 118:

    {
t->insertar((s=new tablasimbolos((yyvsp[(4) - (5)].real),QString((yyvsp[(2) - (5)].TEXT)),"id")),s->getNom());
qDebug()<<t->size();
}
    break;

  case 151:

    {(yyval.real)=(yyvsp[(1) - (3)].real)+(yyvsp[(3) - (3)].real);
            //QString temp=QString::number("%4.1f\n",$$);
			QString temp=QString::number((yyval.real));
			lista.append(temp); }
    break;

  case 152:

    {(yyval.real)=(yyvsp[(1) - (3)].real)-(yyvsp[(3) - (3)].real);
            QString temp=QString::number((yyval.real));
			lista.append(temp);
			
			}
    break;

  case 153:

    {(yyval.real)=(yyvsp[(1) - (1)].real);
            //QString temp=QString::number($$);
			//lista.append(temp);
	//		QString b= QString::number($1);
//t->insertar((s=new tablasimbolos(0,b,"id")),s->getNom());
//qDebug()<<t->size();
			}
    break;

  case 154:

    {(yyval.real)=(yyvsp[(1) - (3)].real)*(yyvsp[(3) - (3)].real);}
    break;

  case 155:

    {(yyval.real)=(yyvsp[(1) - (3)].real)/(yyvsp[(3) - (3)].real);}
    break;

  case 157:

    {(yyval.real)=(yyvsp[(1) - (1)].real);

}
    break;

  case 158:

    {

//s=new  tablasimbolos($1,"numero","a");		
//tablasimbolo->append(*s);
}
    break;

  case 160:

    {(yyval.real)=-(yyvsp[(2) - (2)].real);}
    break;

  case 161:

    {			
//s=new  tablasimbolos($1,"numerod","s");		
//tablasimbolo->append(*s);

}
    break;

  case 162:

    {(yyval.real)=atof((yyvsp[(1) - (1)].TEXT));
//s=new  tablasimbolos(0,QString($1),QString($1));		
//tablasimbolo->append(*s);

}
    break;



      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}





