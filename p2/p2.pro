#-------------------------------------------------
#
# Project created by QtCreator 2015-11-03T15:15:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = p2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scanner.cpp \
    sintactico.cpp \
    tablasimbolos.cpp \
    tsimbolo.cpp \
    ast_expresiones.cpp \
    lista.cpp \
    metodoW.cpp

HEADERS  += mainwindow.h \
    scanner.h \
    sintactico.h \
    tablasimbolos.h \
    tsimbolo.h \
    ast_expresiones.h \
    lista.h \
    metodoW.h

FORMS    += mainwindow.ui

INCLUDEPATH += C:\Users\SAMSUNG\Desktop\graphics para c\winbgim
INCLUDEPATH += C:\Users\SAMSUNG\Desktop\graphics para c\graphics
LIBS +=C:\Users\SAMSUNG\Desktop\graphics para c\libbgi.a
