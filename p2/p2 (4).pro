#-------------------------------------------------
#
# Project created by QtCreator 2015-11-03T15:15:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = p2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scanner.cpp \
    sintactico.cpp \
    tablasimbolos.cpp \
    tsimbolo.cpp

HEADERS  += mainwindow.h \
    scanner.h \
    sintactico.h \
    tablasimbolos.h \
    tsimbolo.h

FORMS    += mainwindow.ui
