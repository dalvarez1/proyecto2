/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 1 "sintactico.y"

#include "scanner.h"//se importa el header del analisis sintactico

#include <iostream> //libreria para imprimir en cosola de C

#include <QString> //libreria para manejo de STRINGS de QT

#include <QHash> //Libreria para manejar HASH TABLES de QT, se usa para la tabla de simbolos

#include <QTextEdit> //libreria QTextEdit de QT para poder mostrar el resultado en pantalla
#include <QLinkedList>

QLinkedList<QString> lista;

int yyerror(const char* mens){
//metodo que se llama al haber un error sintactico
//SE IMPRIME EN CONSOLA EL ERROR
std::cout <<mens<<" "<<yytext<< std::endl;
return 0;
}

QLinkedList<QString> getlita(){
return lista;
}


/* Line 371 of yacc.c  */
#line 95 "sintactico.cpp"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sintactico.h".  */
#ifndef YY_YY_SINTACTICO_H_INCLUDED
# define YY_YY_SINTACTICO_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mas = 258,
     menos = 259,
     por = 260,
     divis = 261,
     mod = 262,
     coma = 263,
     igual = 264,
     print = 265,
     pcoma = 266,
     aparen = 267,
     cparen = 268,
     cllave = 269,
     allave = 270,
     menor = 271,
     mayor = 272,
     menori = 273,
     mayori = 274,
     iguali = 275,
     dif = 276,
     diferente = 277,
     Or = 278,
     And = 279,
     IF = 280,
     Else = 281,
     Case = 282,
     Switch = 283,
     For = 284,
     While = 285,
     Break = 286,
     Class = 287,
     draw = 288,
     resize = 289,
     rotate = 290,
     move = 291,
     circunscribe = 292,
     erase = 293,
     comilla = 294,
     p = 295,
     Variables = 296,
     Figures = 297,
     Design = 298,
     Int = 299,
     String = 300,
     Float = 301,
     Arreglo = 302,
     textura = 303,
     blue = 304,
     orange = 305,
     red = 306,
     brownie = 307,
     green = 308,
     yellow = 309,
     white = 310,
     black = 311,
     skyblue = 312,
     grey = 313,
     triangulo = 314,
     dosp = 315,
     New = 316,
     ovalo = 317,
     poligono = 318,
     circulo = 319,
     flecha = 320,
     rectangulo = 321,
     nube = 322,
     punto = 323,
     default = 324,
     texto = 325,
     linea = 326,
     imagen = 327,
     iden = 328,
     Cadena = 329,
     acorch = 330,
     ccorch = 331,
     numerod = 332,
     numero = 333
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 30 "sintactico.y"

//se especifican los tipo de valores para los no terminales y lo terminales
char TEXT [256];
float real;


/* Line 387 of yacc.c  */
#line 223 "sintactico.cpp"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_SINTACTICO_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 251 "sintactico.cpp"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   514

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  79
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  27
/* YYNRULES -- Number of rules.  */
#define YYNRULES  120
/* YYNRULES -- Number of states.  */
#define YYNSTATES  294

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   333

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     9,    14,    19,    24,    29,    34,    39,
      43,    46,    48,    56,    65,    74,    86,    97,    99,   111,
     119,   129,   138,   142,   146,   156,   165,   170,   175,   180,
     185,   191,   196,   201,   206,   211,   214,   220,   224,   226,
     227,   229,   231,   233,   235,   237,   240,   242,   244,   248,
     249,   252,   256,   260,   264,   268,   272,   276,   278,   280,
     284,   288,   290,   293,   296,   298,   300,   305,   310,   316,
     322,   324,   333,   337,   341,   345,   349,   355,   361,   367,
     371,   373,   375,   379,   381,   383,   387,   389,   391,   397,
     401,   405,   411,   413,   419,   423,   427,   429,   435,   449,
     461,   465,   469,   473,   479,   481,   487,   491,   495,   497,
     501,   505,   507,   511,   515,   519,   521,   523,   527,   530,
     532
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      80,     0,    -1,    32,    73,    15,    81,    14,    -1,    32,
      15,    81,    14,    -1,    41,    15,    14,    81,    -1,    42,
      15,    14,    81,    -1,    43,    15,    14,    81,    -1,    41,
      15,    91,    14,    -1,    42,    15,    82,    14,    -1,    43,
      15,    14,    -1,    82,    83,    -1,    83,    -1,    25,    12,
      87,    13,    15,    82,    14,    -1,    25,    12,    87,    13,
      15,    82,    14,    86,    -1,    25,    12,    87,    13,    15,
      82,    14,    86,    -1,    25,    12,    87,    13,    15,    82,
      14,    26,    15,    82,    14,    -1,    28,    12,    73,    13,
      15,    84,    69,    60,    82,    14,    -1,   103,    -1,    29,
      12,    91,    11,    87,    11,    85,    13,    15,    82,    14,
      -1,    30,    12,    87,    13,    15,    82,    14,    -1,    27,
      73,    60,    15,    82,    31,    11,    14,    84,    -1,    27,
      73,    60,    15,    82,    31,    11,    14,    -1,    73,     3,
       3,    -1,    73,     4,     4,    -1,    26,    25,    12,    87,
      13,    15,    82,    14,    86,    -1,    26,    25,    12,    87,
      13,    15,    82,    14,    -1,    26,    15,    82,    14,    -1,
      89,    19,    89,    87,    -1,    89,    18,    89,    87,    -1,
      89,    20,    89,    87,    -1,    89,    21,     9,    89,    87,
      -1,    89,    16,    89,    87,    -1,    89,    17,    89,    87,
      -1,    89,    23,    89,    87,    -1,    89,    24,    89,    87,
      -1,    88,    87,    -1,    12,    87,    13,    88,    87,    -1,
      12,    87,    13,    -1,    73,    -1,    -1,    19,    -1,    18,
      -1,    17,    -1,    16,    -1,    20,    -1,    21,     9,    -1,
      23,    -1,    24,    -1,    12,    88,    13,    -1,    -1,     4,
      89,    -1,    89,     3,    89,    -1,    89,     4,    89,    -1,
      89,     5,    89,    -1,    89,     6,    89,    -1,    89,     7,
      89,    -1,    12,    89,    13,    -1,    78,    -1,    73,    -1,
      90,     3,    90,    -1,    39,    73,    39,    -1,    73,    -1,
      93,    91,    -1,    94,    91,    -1,    93,    -1,    94,    -1,
      73,     9,   103,    11,    -1,    73,     9,    90,    11,    -1,
      73,     9,   103,    11,    91,    -1,    73,     9,    90,    11,
      91,    -1,    92,    -1,    48,    73,     9,    61,    48,    12,
      13,    11,    -1,    44,    95,    11,    -1,    45,    96,    11,
      -1,    46,    97,    11,    -1,    73,     9,   103,    -1,    44,
      73,     9,    78,    11,    -1,    45,    73,     9,    90,    11,
      -1,    46,    73,     9,    77,    11,    -1,    73,     8,    95,
      -1,    73,    -1,    98,    -1,    73,     8,    96,    -1,    73,
      -1,    99,    -1,    73,     8,    97,    -1,    73,    -1,   100,
      -1,    73,     9,    78,     8,    98,    -1,    73,     8,    98,
      -1,    73,     9,   103,    -1,    73,     9,   103,     8,    98,
      -1,    73,    -1,    73,     9,    90,     8,    99,    -1,    73,
       8,    99,    -1,    73,     9,    90,    -1,    73,    -1,    73,
       9,    77,     8,   100,    -1,    73,    75,   103,    76,    75,
     103,    76,     9,    75,   101,    76,     8,   100,    -1,    73,
      75,   103,    76,    75,   103,    76,     9,    75,   101,    76,
      -1,    73,     9,    77,    -1,    73,     8,   100,    -1,    73,
       9,   103,    -1,    73,     9,   103,     8,   100,    -1,    73,
      -1,    12,   102,    13,     8,   101,    -1,    12,   102,    13,
      -1,    78,     8,   102,    -1,    78,    -1,   103,     3,   104,
      -1,   103,     4,   104,    -1,   104,    -1,   104,     5,   105,
      -1,   104,     6,   105,    -1,   104,     7,   105,    -1,   105,
      -1,    78,    -1,    12,   103,    13,    -1,     4,   103,    -1,
      77,    -1,    73,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   150,   150,   151,   153,   154,   155,   156,   157,   158,
     160,   161,   163,   164,   165,   166,   167,   168,   169,   170,
     174,   175,   178,   178,   180,   181,   182,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     201,   202,   203,   204,   205,   206,   207,   208,   209,   210,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   226,
     227,   228,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   241,   243,   244,   245,   246,   249,   250,   251,   253,
     254,   255,   257,   258,   259,   261,   262,   263,   265,   266,
     267,   268,   269,   271,   272,   273,   274,   276,   277,   278,
     279,   280,   281,   282,   283,   285,   286,   289,   290,   293,
     297,   300,   304,   305,   306,   307,   309,   310,   311,   312,
     313
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "mas", "menos", "por", "divis", "mod",
  "coma", "igual", "print", "pcoma", "aparen", "cparen", "cllave",
  "allave", "menor", "mayor", "menori", "mayori", "iguali", "dif",
  "diferente", "Or", "And", "IF", "Else", "Case", "Switch", "For", "While",
  "Break", "Class", "draw", "resize", "rotate", "move", "circunscribe",
  "erase", "comilla", "p", "Variables", "Figures", "Design", "Int",
  "String", "Float", "Arreglo", "textura", "blue", "orange", "red",
  "brownie", "green", "yellow", "white", "black", "skyblue", "grey",
  "triangulo", "dosp", "New", "ovalo", "poligono", "circulo", "flecha",
  "rectangulo", "nube", "punto", "default", "texto", "linea", "imagen",
  "iden", "Cadena", "acorch", "ccorch", "numerod", "numero", "$accept",
  "INICIO", "CUERPO", "INSTRUCCIONES", "INSTRUCCION", "CASOS",
  "INCREMENTO", "ELSI", "EXPRESIONLOGICA", "EXPRE", "EXPRESIONNUMERICA",
  "EXPRESIONCADENA", "ASIGNACION", "TEXTURA", "DECLARACION", "ASIGNAR",
  "ASIGM", "ASIGM1", "ASIGM2", "VALORES", "VALORES1", "VALORES2",
  "ARREGLO", "A", "E", "T", "F", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    79,    80,    80,    81,    81,    81,    81,    81,    81,
      82,    82,    83,    83,    83,    83,    83,    83,    83,    83,
      84,    84,    85,    85,    86,    86,    86,    87,    87,    87,
      87,    87,    87,    87,    87,    87,    87,    87,    87,    87,
      88,    88,    88,    88,    88,    88,    88,    88,    88,    88,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    90,
      90,    90,    91,    91,    91,    91,    91,    91,    91,    91,
      91,    92,    93,    93,    93,    93,    94,    94,    94,    95,
      95,    95,    96,    96,    96,    97,    97,    97,    98,    98,
      98,    98,    98,    99,    99,    99,    99,   100,   100,   100,
     100,   100,   100,   100,   100,   101,   101,   102,   102,   103,
     103,   103,   104,   104,   104,   104,   105,   105,   105,   105,
     105
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     5,     4,     4,     4,     4,     4,     4,     3,
       2,     1,     7,     8,     8,    11,    10,     1,    11,     7,
       9,     8,     3,     3,     9,     8,     4,     4,     4,     4,
       5,     4,     4,     4,     4,     2,     5,     3,     1,     0,
       1,     1,     1,     1,     1,     2,     1,     1,     3,     0,
       2,     3,     3,     3,     3,     3,     3,     1,     1,     3,
       3,     1,     2,     2,     1,     1,     4,     4,     5,     5,
       1,     8,     3,     3,     3,     3,     5,     5,     5,     3,
       1,     1,     3,     1,     1,     3,     1,     1,     5,     3,
       3,     5,     1,     5,     3,     3,     1,     5,    13,    11,
       3,     3,     3,     5,     1,     5,     3,     3,     1,     3,
       3,     1,     3,     3,     3,     1,     1,     3,     2,     1,
       1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     0,     1,     0,     0,     0,     0,
       0,     0,     0,     0,     3,     0,     0,     0,     0,     0,
       0,     0,     0,    70,    64,    65,     0,     0,     0,     0,
       0,     0,     0,   120,   119,   116,     0,    11,    17,   111,
     115,     9,     2,     4,    80,     0,    81,    83,     0,    84,
      86,     0,    87,     0,     0,     7,    62,    63,   118,     0,
       5,    39,     0,     0,    39,     8,    10,     0,     0,     0,
       0,     0,     6,     0,     0,    72,     0,     0,    73,     0,
       0,     0,    74,     0,     0,   120,     0,    75,   117,     0,
      39,    43,    42,    41,    40,    44,     0,    46,    47,    58,
      57,     0,    39,     0,     0,     0,     0,   109,   110,   112,
     113,   114,    80,    79,    81,   116,    90,    83,    82,    84,
      61,     0,    86,    85,    87,   119,   102,     0,     0,     0,
       0,    67,    66,     0,    58,    50,     0,     0,     0,    45,
       0,    35,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    39,     0,     0,     0,
      76,     0,     0,     0,    77,     0,     0,    78,     0,     0,
       0,    60,    59,    69,    68,     0,    49,    48,    56,     0,
      51,    52,    53,    54,    55,    39,    39,    39,    39,    39,
       0,    39,    39,     0,     0,     0,   116,    92,    88,    91,
      95,    96,    93,   119,   104,    97,   103,     0,     0,    49,
      39,     0,     0,    31,    32,    28,    27,    29,    39,    33,
      34,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    36,    12,    50,    30,     0,     0,     0,     0,    19,
      89,    94,   101,     0,    71,     0,    13,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    22,    23,     0,
       0,     0,    39,     0,    16,     0,     0,     0,    15,     0,
       0,    18,   108,     0,    99,     0,     0,     0,   106,     0,
       0,    21,   107,     0,    98,     0,    20,   105,    25,     0,
      24,     0,     0,    26
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     9,    36,    37,   222,   238,   246,   141,   102,
     103,    86,    22,    23,    24,    25,    45,    48,    51,    46,
      49,    52,   267,   273,    38,    39,    40
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -172
static const yytype_int16 yypact[] =
{
      -5,    14,    32,   234,    42,  -172,    84,    95,   102,   105,
     234,   318,   181,   121,  -172,   127,   234,    48,    92,   114,
     119,   185,   182,  -172,   353,   353,    27,    27,   234,   188,
     202,   209,   212,  -172,  -172,  -172,   187,  -172,    31,   365,
    -172,   234,  -172,  -172,   210,   197,  -172,   236,   226,  -172,
      43,   228,  -172,   189,    69,  -172,  -172,  -172,  -172,   136,
    -172,   160,   168,   353,   160,  -172,  -172,    27,    27,    27,
      27,    27,  -172,   178,   201,  -172,   180,   -17,  -172,   184,
     305,    27,  -172,   194,   199,    80,   103,   186,  -172,    34,
     160,  -172,  -172,  -172,  -172,  -172,   253,  -172,  -172,    10,
    -172,   271,   160,   453,   328,   259,   338,   365,   365,  -172,
    -172,  -172,   281,  -172,  -172,    50,   232,   284,  -172,  -172,
    -172,   223,    70,  -172,  -172,   152,   310,    22,   275,   289,
     -17,   353,   353,    34,  -172,   370,   352,    24,   431,  -172,
     333,  -172,    34,    34,    34,    34,    34,    34,    34,    34,
      34,    34,   359,    34,    34,   369,   160,   372,   346,   334,
    -172,   334,   -17,   339,  -172,   355,   349,  -172,   349,   331,
     399,  -172,  -172,  -172,  -172,   485,   397,  -172,  -172,   327,
     370,   370,   354,   436,   436,   150,   150,   150,   150,   150,
      34,   150,   150,   398,   416,   327,   421,   294,  -172,  -172,
     135,   371,  -172,   196,   107,  -172,  -172,    27,   417,   463,
     160,   218,    34,  -172,  -172,  -172,  -172,  -172,   150,  -172,
    -172,   358,   376,   373,   238,   334,   339,   349,    46,   442,
     448,  -172,   437,     7,  -172,   402,   404,   386,   452,  -172,
    -172,  -172,  -172,   457,  -172,    61,  -172,   470,   327,   464,
     474,   478,   393,   327,   482,   327,   257,  -172,  -172,   327,
     483,   269,   160,   276,  -172,   296,   418,   423,  -172,   484,
     489,  -172,   493,   490,   494,   491,   495,   418,   496,   349,
     327,   398,  -172,   483,  -172,   308,  -172,  -172,   479,    88,
    -172,   327,   315,  -172
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -172,  -172,   120,  -171,   -36,   227,  -172,   219,   -60,   -75,
     -79,   -68,    -8,  -172,  -172,  -172,   438,   434,   433,   -66,
     -74,   -76,   230,   237,   -21,   335,   325
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -101
static const yytype_int16 yytable[] =
{
      66,   101,   119,   124,   106,    58,    59,   114,   211,   121,
     135,   138,   144,   145,   146,   137,    56,    57,   -52,   -52,
     -52,   -38,    84,   -38,   224,    67,    68,     1,    89,     3,
     136,    26,     5,    87,    67,    68,    90,   177,    89,    27,
      91,    92,    93,    94,    95,    96,   133,    97,    98,    67,
      68,    79,    80,   116,   175,   105,   120,    10,   159,   126,
     127,   160,   172,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,    26,   191,   192,   253,   256,    79,   165,
     -52,    27,   261,   -61,   263,   -52,   254,     4,   265,   202,
     205,   -61,   206,   198,   200,   199,   194,    99,   169,    11,
      33,   210,   100,   291,    34,    35,   130,   134,    84,   285,
      12,   218,   100,   254,   131,   227,   165,    13,    81,    14,
     292,    44,   243,   173,   174,   213,   214,   215,   216,   217,
      15,   219,   220,   233,   230,    41,    43,   116,   130,    67,
      68,    42,    85,   163,   126,    81,    34,    35,    60,    88,
     231,   242,   241,   142,   212,   144,   145,   146,   234,   240,
     166,    72,    90,   167,    89,    47,    91,    92,    93,    94,
      95,    96,    90,    97,    98,    66,    91,    92,    93,    94,
      95,    96,    81,    97,    98,    26,   228,    50,    66,    67,
      68,    26,    53,    27,    54,    28,    55,   132,    83,    27,
      61,    65,   269,   284,   166,    26,    29,  -100,    75,    30,
      31,    32,    29,    27,    62,    30,    31,    32,    73,    74,
      66,    63,    26,    99,    64,    66,   130,    66,   100,    66,
      27,   163,   232,    99,   164,    67,    68,    78,   100,    82,
     161,   104,    26,    29,    76,    77,    30,    31,    32,    66,
      27,   112,   239,   117,    33,   128,    66,   122,    34,    35,
      33,    26,   139,    29,    34,    35,    30,    31,    32,    27,
     156,   264,   129,    26,    33,     6,     7,     8,    34,   115,
      26,    27,    29,   268,   140,    30,    31,    32,    27,    73,
     158,    33,    76,   162,    29,    34,    35,    30,    31,    32,
      26,    29,   225,   158,    30,    31,    32,   270,    27,    26,
     271,    33,    26,    67,    68,    34,    35,    27,   168,    26,
      27,    29,   288,   170,    30,    31,    32,    27,   171,   293,
      33,    26,    16,    29,    34,    35,    30,    31,    32,    27,
      29,   155,    33,    30,    31,    32,    34,    35,   179,    33,
      26,   157,    29,    34,    35,    30,    31,    32,    27,    26,
     145,   146,    17,    18,    19,   176,    20,    27,   190,    33,
      69,    70,    71,    34,    35,   144,   145,   146,    33,   226,
     162,    33,   125,    35,   193,    34,    35,   195,    33,   249,
     250,    21,    34,    35,   109,   110,   111,    17,    18,    19,
      33,    20,   107,   108,    34,    35,   207,   197,   -37,   209,
     -37,   208,   201,    91,    92,    93,    94,    95,    96,    33,
      97,    98,   204,    34,   196,   221,    21,   223,    33,   159,
     229,   235,   203,    35,   142,   143,   144,   145,   146,   142,
     143,   144,   145,   146,   178,   236,   237,   147,   148,   149,
     150,   151,   152,   244,   153,   154,   142,   143,   144,   145,
     146,   177,   247,   245,   248,   251,   252,   257,   260,   147,
     148,   149,   150,   151,   152,   209,   153,   154,   258,    91,
      92,    93,    94,    95,    96,   255,    97,    98,   142,   143,
     144,   145,   146,   259,   262,   266,   272,   275,   178,   274,
     276,   277,   279,   278,   283,   289,   280,   290,   286,   281,
     118,   113,   123,   287,   282
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-172)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_uint16 yycheck[] =
{
      36,    61,    76,    79,    64,    26,    27,    73,   179,    77,
      89,    90,     5,     6,     7,    90,    24,    25,    11,    12,
      13,    11,    39,    13,   195,     3,     4,    32,     4,    15,
      90,     4,     0,    54,     3,     4,    12,    13,     4,    12,
      16,    17,    18,    19,    20,    21,    12,    23,    24,     3,
       4,     8,     9,    74,   133,    63,    73,    15,     8,    80,
      81,    11,   130,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,     4,   153,   154,    15,   248,     8,     9,
      73,    12,   253,     3,   255,    78,    25,    73,   259,   163,
     166,    11,   168,   159,   162,   161,   156,    73,    76,    15,
      73,   176,    78,    15,    77,    78,     3,    73,    39,   280,
      15,   190,    78,    25,    11,     8,     9,    15,    75,    14,
     291,    73,    76,   131,   132,   185,   186,   187,   188,   189,
      10,   191,   192,   212,   209,    14,    16,   158,     3,     3,
       4,    14,    73,     8,   165,    75,    77,    78,    28,    13,
     210,   227,   226,     3,     4,     5,     6,     7,   218,   225,
       8,    41,    12,    11,     4,    73,    16,    17,    18,    19,
      20,    21,    12,    23,    24,   211,    16,    17,    18,    19,
      20,    21,    75,    23,    24,     4,   207,    73,   224,     3,
       4,     4,    73,    12,     9,    14,    14,    11,     9,    12,
      12,    14,   262,   279,     8,     4,    25,    11,    11,    28,
      29,    30,    25,    12,    12,    28,    29,    30,     8,     9,
     256,    12,     4,    73,    12,   261,     3,   263,    78,   265,
      12,     8,    14,    73,    11,     3,     4,    11,    78,    11,
       8,    73,     4,    25,     8,     9,    28,    29,    30,   285,
      12,    73,    14,    73,    73,    61,   292,    73,    77,    78,
      73,     4,     9,    25,    77,    78,    28,    29,    30,    12,
      11,    14,    73,     4,    73,    41,    42,    43,    77,    78,
       4,    12,    25,    14,    13,    28,    29,    30,    12,     8,
       9,    73,     8,     9,    25,    77,    78,    28,    29,    30,
       4,    25,     8,     9,    28,    29,    30,    31,    12,     4,
      14,    73,     4,     3,     4,    77,    78,    12,     8,     4,
      12,    25,    14,    48,    28,    29,    30,    12,    39,    14,
      73,     4,    14,    25,    77,    78,    28,    29,    30,    12,
      25,    13,    73,    28,    29,    30,    77,    78,    15,    73,
       4,    13,    25,    77,    78,    28,    29,    30,    12,     4,
       6,     7,    44,    45,    46,    13,    48,    12,     9,    73,
       5,     6,     7,    77,    78,     5,     6,     7,    73,     8,
       9,    73,    77,    78,    15,    77,    78,    15,    73,     3,
       4,    73,    77,    78,    69,    70,    71,    44,    45,    46,
      73,    48,    67,    68,    77,    78,    75,    73,    11,    12,
      13,    12,    73,    16,    17,    18,    19,    20,    21,    73,
      23,    24,    73,    77,    78,    27,    73,    11,    73,     8,
      13,    73,    77,    78,     3,     4,     5,     6,     7,     3,
       4,     5,     6,     7,    13,    69,    73,    16,    17,    18,
      19,    20,    21,    11,    23,    24,     3,     4,     5,     6,
       7,    13,    60,    26,    60,    13,     9,     3,    75,    16,
      17,    18,    19,    20,    21,    12,    23,    24,     4,    16,
      17,    18,    19,    20,    21,    15,    23,    24,     3,     4,
       5,     6,     7,    15,    12,    12,    78,    13,    13,    76,
      11,     8,     8,    13,     8,    26,    15,   288,   281,    14,
      76,    73,    79,   283,   277
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    32,    80,    15,    73,     0,    41,    42,    43,    81,
      15,    15,    15,    15,    14,    81,    14,    44,    45,    46,
      48,    73,    91,    92,    93,    94,     4,    12,    14,    25,
      28,    29,    30,    73,    77,    78,    82,    83,   103,   104,
     105,    14,    14,    81,    73,    95,    98,    73,    96,    99,
      73,    97,   100,    73,     9,    14,    91,    91,   103,   103,
      81,    12,    12,    12,    12,    14,    83,     3,     4,     5,
       6,     7,    81,     8,     9,    11,     8,     9,    11,     8,
       9,    75,    11,     9,    39,    73,    90,   103,    13,     4,
      12,    16,    17,    18,    19,    20,    21,    23,    24,    73,
      78,    87,    88,    89,    73,    91,    87,   104,   104,   105,
     105,   105,    73,    95,    98,    78,   103,    73,    96,    99,
      73,    90,    73,    97,   100,    77,   103,   103,    61,    73,
       3,    11,    11,    12,    73,    89,    87,    88,    89,     9,
      13,    87,     3,     4,     5,     6,     7,    16,    17,    18,
      19,    20,    21,    23,    24,    13,    11,    13,     9,     8,
      11,     8,     9,     8,    11,     9,     8,    11,     8,    76,
      48,    39,    90,    91,    91,    89,    13,    13,    13,    15,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
       9,    89,    89,    15,    87,    15,    78,    73,    98,    98,
      90,    73,    99,    77,    73,   100,   100,    75,    12,    12,
      88,    82,     4,    87,    87,    87,    87,    87,    89,    87,
      87,    27,    84,    11,    82,     8,     8,     8,   103,    13,
      88,    87,    14,    89,    87,    73,    69,    73,    85,    14,
      98,    99,   100,    76,    11,    26,    86,    60,    60,     3,
       4,    13,     9,    15,    25,    15,    82,     3,     4,    15,
      75,    82,    12,    82,    14,    82,    12,   101,    14,    87,
      31,    14,    78,   102,    76,    13,    11,     8,    13,     8,
      15,    14,   102,     8,   100,    82,    84,   101,    14,    26,
      86,    15,    82,    14
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 109:
/* Line 1792 of yacc.c  */
#line 293 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (3)].real)+(yyvsp[(3) - (3)].real);
            //QString temp=QString::number("%4.1f\n",$$);
			QString temp=QString::number((yyval.real));
			lista.append(temp); }
    break;

  case 110:
/* Line 1792 of yacc.c  */
#line 297 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (3)].real)-(yyvsp[(3) - (3)].real);
            QString temp=QString::number((yyval.real));
			lista.append(temp);}
    break;

  case 111:
/* Line 1792 of yacc.c  */
#line 300 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (1)].real);
            QString temp=QString::number((yyval.real));
			lista.append(temp);}
    break;

  case 112:
/* Line 1792 of yacc.c  */
#line 304 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (3)].real)*(yyvsp[(3) - (3)].real);}
    break;

  case 113:
/* Line 1792 of yacc.c  */
#line 305 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (3)].real)/(yyvsp[(3) - (3)].real);}
    break;

  case 115:
/* Line 1792 of yacc.c  */
#line 307 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (1)].real);}
    break;

  case 116:
/* Line 1792 of yacc.c  */
#line 309 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (1)].real);}
    break;

  case 119:
/* Line 1792 of yacc.c  */
#line 312 "sintactico.y"
    {(yyval.real)=(yyvsp[(1) - (1)].real);}
    break;


/* Line 1792 of yacc.c  */
#line 1804 "sintactico.cpp"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


/* Line 2055 of yacc.c  */
#line 314 "sintactico.y"

